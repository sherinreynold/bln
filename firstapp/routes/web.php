<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',function () {

return view('home');
});

Route::get('/about',function () {

    return view('about');
    });

    Route::get('/contact',function () {

        return view('contact');
        }); 

    Route::post('/contact','MessagesController@save'); 

    Route::get('/messages','MessagesController@get'); 

    Route::get('/complaints','ComplaintsController@get'); 

    Route::get('/add_complaints','ComplaintsController@add'); 

    Route::post('/add_complaints','ComplaintsController@save');

    Route::resource('todo','TodosController');
