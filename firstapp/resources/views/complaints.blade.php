@extends('layouts.app')

@section('title')
|complaints
@endsection

@section('content')
<h1>Complaints</h1>

{!! Form::open(['url' => '/add_complaints']) !!}

<div class="form-group"> 
{{form::label('name','Name')}}
{{form::text('name','',['class'=>'form-control','placeholder'=>'Enter your Name'])}}
</div>

<div class="form-group"> 
{{form::label('email','Email')}}
{{form::text('email','',['class'=>'form-control','placeholder'=>'Enter your Email'])}}
</div>

<div class="form-group"> 
{{form::label('complaints','Complaints')}}
{{form::textarea('complaints','',['class'=>'form-control','placeholder'=>'Enter your Complaints'])}}

</div>

<div class="form-group"> 


{{form::submit('Submit',['class'=>'btn btn-primary'])}}
</div>



{!! Form::close()  !!}
@endsection

