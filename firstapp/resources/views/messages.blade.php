@extends('layouts.app')

@section('title')
| Messages
@endsection

@section('content')
<h1>Messages from </h1>

@if(count($messages)>0)
<ul>
@foreach($messages as $m)

<li>{{$m->name}}</li>
@endforeach
@endif
</ul>
@endsection

