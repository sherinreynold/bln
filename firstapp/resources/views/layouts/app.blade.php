<html>
<head>
<title>First App @yield('title') </title>

<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >

</head>
<body>

@include('inc.navbar')

@if(Request::is('/') || Request::is('about'))
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
  <div class="col-md-5 p-lg-5 mx-auto my-5">
    <h1 class="display-4 font-weight-normal">FirstApp</h1>
    @if(Request::is('/'))
    <a href="/todo" class="btn btn-primary">Todo List</a>
    @else
    <p class="lead font-weight-normal"> Welcome to FirstApp.This is my first laravel App.</p>
    
    @endif
    
  </div>
  <div class="product-device shadow-sm d-none d-md-block"></div>
  <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>
@endif

<div class="row">  

<div class="col-md-8 col-lg-8">

@if(count($errors)>0)

@foreach($errors->all() as $error)
<div class="alert alert-danger">
{{$error}}
</div>
@endforeach

@endif

@if(session('success'))
<div class='alert alert-success'>
{{session('success')}}
</div>
@endif

@yield('content')
</div>
<!-- <div class="col-md-4 col-lg-4" >
@include('inc.sidebar')
</div> -->

</div>

<footer id="footer" class="text-center">
copyright 2019 &copy FirstApp
</footer>
</body>

</html>