@extends('layouts.app')

@section('title')
| Complaints
@endsection

@section('content')
<h1>Complaints </h1>

@if(count($complaints)>0)
<ul>
@foreach($complaints as $m)

<li>{{$m->name}}</li>
@endforeach
@endif
</ul>
@endsection

