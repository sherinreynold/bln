@extends('layouts.app')

@section('title')
|contact
@endsection

@section('content')
<h1>Contact us</h1>

{!! Form::open(['url' => '/contact']) !!}

<div class="form-group"> 
{{form::label('name','Name')}}
{{form::text('name','',['class'=>'form-control','placeholder'=>'Enter your Name'])}}
</div>

<div class="form-group"> 
{{form::label('email','Email')}}
{{form::text('email','',['class'=>'form-control','placeholder'=>'Enter your Email'])}}
</div>

<div class="form-group"> 
{{form::label('message','Message')}}
{{form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter your Message'])}}

</div>

<div class="form-group"> 


{{form::submit('Submit',['class'=>'btn btn-primary'])}}
</div>



{!! Form::close()  !!}
@endsection

