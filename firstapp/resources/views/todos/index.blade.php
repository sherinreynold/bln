@extends('layouts.app')

@section('content')
<h3 class="text-center">My Todos  <a href="/todo/create" class="btn btn-primary">+</a></h3>

@if(count($todos) > 0)

@foreach($todos as $todo)

<div class="well">
<h4>{{$todo->text}}</h4>
<span class="label label-danger">{{$todo->due}}</span>
</div>

@endforeach


@endif
@endsection