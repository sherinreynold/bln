<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complaint;

class ComplaintsController extends Controller
{
    public function get(){
        $complaints=Complaint::all();
        return view('view_complaints')->with('complaints',$complaints);

    }
    public function add(){
        return view('complaints');
    }

    public function save(Request $request){
       
        $this->validate($request,['name'=>'required','email'=>'required']);

        //save
        $message=new Complaint;
        $message->name=$request->input('name');
        $message->email=$request->input('email');
        $message->complaints=$request->input('complaints');

        $message->save();
        return( redirect('/')->with('success','Message Addedd Successfully.') );
    }
}
