<?php
namespace App\Http\Controllers\frontend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RedGenies;
use App\Models\SalesGenies;
use App\Models\Customers;
use App\Models\RedPoints;
use App\Models\Settings;
use App\Frontend;
use App\Http\Controllers\Controller;
class RedPointsController extends Controller
{
 
    public function add(Request $request){
        
        
        if(isset($_SERVER['HTTP_R2G_SECRET_ACCESS_KEY']) && $_SERVER['HTTP_R2G_SECRET_ACCESS_KEY']==123){
            
            if($request->post('referral_code') && $request->post('customer_id') && $request->post('invoice_amount')
            && $request->post('invoice_number') && $request->post('currency') && $request->post('invoice_referrence_id')){

            
             $invoice_amount=$request->post('invoice_amount');
             $invoice_number=$request->post('invoice_number');
             $currency=$request->post('currency');
             $invoice_referrence_id=$request->post('invoice_referrence_id');
             
             //amt % calculation sections
              $ref_percentage=60;
              $ref_parent_percentage=25;
              $ref_grand_parent_percentage=15;

              $pay_out_info=Settings::where('id', '=',1)->get()->ToArray();
              if(!empty($pay_out_info)){

                $pay_out_percentage=$pay_out_info[0]['value'];
                $pay_out_value=$invoice_amount*$pay_out_percentage/100;
                //echo $pay_out_value;exit;
                //echo $pay_out_percentage;exit;

              }else{
                  echo 'Pay out percent is not set';exit;
              }

              //amt % calculation sections

            $ref_code=$request->post('referral_code');
            $c_info = Customers::where('code', '=', $ref_code)->get()->ToArray();
            //var_dump($c_info);exit;
            if(!empty($c_info)){

            
           $reffer_by_code=$c_info[0]['ref_code'];
           $reffer_by_info=SalesGenies::where('code', '=', $reffer_by_code)->get()->ToArray();
           
           $reffer_by_id=$reffer_by_info[0]['fk_frontend_user_id'];
           //if parent is redgenie
           if($reffer_by_info[0]['fk_red_genie_id'] != 0){
           
            $parent_redgenie_info=RedGenies::where('id', '=', $reffer_by_info[0]['fk_red_genie_id'])->get()->ToArray();  
            $reffer_by_id_parent=$parent_redgenie_info[0]['fk_frontend_user_id']; 
            
            //add points parent
            
            if($reffer_by_id_parent){
               
                $r_parent_point=$pay_out_value*$ref_parent_percentage/100;    
            
                $points= new RedPoints();
                $points->refferal_code = $ref_code;
                $points->rl_customer_id =$request->post('customer_id');
                $points->invoice_amount  =$invoice_amount;
                $points->currency  =$currency;
                $points->invoice_no  =$invoice_number;
                $points->invoice_reff_id  =$invoice_referrence_id;
                $points->point  =$r_parent_point;
                $points->pay_out_percentage  =$pay_out_percentage;
                
                $points->fk_frontend_user_id=$reffer_by_id_parent;
                $points->save();
            }


        }
        elseif($reffer_by_info[0]['fk_sales_genie_id'] != 0){
           
            $parent_salesgenie_info=SalesGenies::where('id', '=', $reffer_by_info[0]['fk_sales_genie_id'])->get()->ToArray();  
            $reffer_by_id_parent=$parent_salesgenie_info[0]['fk_frontend_user_id']; 
            //add credits parent
            
            if($reffer_by_id_parent){
            
            $r_parent_point=$pay_out_value*$ref_parent_percentage/100;    
            
            $points= new RedPoints();
            $points->refferal_code = $ref_code;
            $points->rl_customer_id =$request->post('customer_id');
            $points->invoice_amount  =$invoice_amount;
            $points->currency  =$currency;
            $points->invoice_no  =$invoice_number;
            $points->invoice_reff_id  =$invoice_referrence_id;
            $points->point  =$r_parent_point;
            $points->pay_out_percentage  =$pay_out_percentage;
            
            $points->fk_frontend_user_id=$reffer_by_id_parent;
            $points->save();

            }

            //grand parent section if

            $grand_parent_redgenie_info=RedGenies::where('id', '=', $parent_salesgenie_info[0]['fk_red_genie_id'])->get()->ToArray();  
            
            //echo '<pre>';print_r($grand_parent_redgenie_info);exit;
            
            $reffer_by_id_grand=$grand_parent_redgenie_info[0]['fk_frontend_user_id']; 
            
            if($reffer_by_id_grand){
       
            $r_grand_point=$pay_out_value*$ref_grand_parent_percentage/100;    
            
            $points= new RedPoints();
            $points->refferal_code = $ref_code;
            $points->rl_customer_id =$request->post('customer_id');
            $points->invoice_amount  =$invoice_amount;
            $points->currency  =$currency;
            $points->invoice_no  =$invoice_number;
            $points->invoice_reff_id  =$invoice_referrence_id;
            $points->point  =$r_grand_point;
            $points->pay_out_percentage  =$pay_out_percentage;
            
            $points->fk_frontend_user_id=$reffer_by_id_grand;
            $points->save();

                }

        }


        $r_point=$pay_out_value*$ref_percentage/100;

        if($reffer_by_id){
        $points= new RedPoints();
            $points->refferal_code = $ref_code;
            $points->rl_customer_id =$request->post('customer_id');
            $points->invoice_amount  =$invoice_amount;
            $points->currency  =$currency;
            $points->invoice_no  =$invoice_number;
            $points->invoice_reff_id  =$invoice_referrence_id;
            $points->point  =$r_point;
            $points->pay_out_percentage  =$pay_out_percentage;
            
            $points->fk_frontend_user_id=$reffer_by_id;
            $points->save();
        }

            $resp['status']='true';
            $resp['info']='Refferal point addedd successfully';
            $resp['result']=array('customer_id'=>$request->post('customer_id'));

            echo json_encode($resp);exit;

            }

        }else{echo 'parameters missing';}
            }
            else{
                echo 'no access';
            }


        
    }
   
}