<?php

namespace App\Http\Controllers\frontend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RedGenies;
use App\Models\SalesGenies;
use App\Models\Customers;
use App\Frontend;
use App\Models\Experiences;
use App\Models\RedPoints;
use App\Models\RedCredits;
use App\Http\Controllers\Controller;
class StatementController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

public function index(Request $request){

    $login_info = auth()->guard('frontend')->user()->ToArray();

    if($login_info['user_type'] == 'customer'){


        if($request->input('start_date') &&  $request->input('end_date')){

            $from = date($request->input('start_date'));
            $to = date($request->input('end_date'));
            $points = RedPoints::join('customer as c','c.code','=','red_points.refferal_code')
                                 ->where('c.fk_frontend_user_id',$login_info['id'])
                                 ->whereBetween('created_at', [$from, $to])
                                 ->groupBy('red_points.invoice_no')
                                 ->get(['red_points.*'])->ToArray();
        
        }
        else{
            
            $points = RedPoints::join('customer as c','c.code','=','red_points.refferal_code')
                                 ->where('c.fk_frontend_user_id',$login_info['id'])
                                 ->groupBy('red_points.invoice_no')
                                 ->get(['red_points.*'])->ToArray();

            //echo '<pre>';print_r($points);exit;
        
        }
        
        return view('frontend.customers.statements_customer')->with(['login_info'=>$login_info,'points'=>$points]);


    }else{



        $points = RedPoints::where('fk_frontend_user_id',$login_info['id']);

        if($request->has('start_date') && $request->has('end_date') && $request->input('start_date') !='' && $request->input('end_date') != ''){

        $from = date($request->input('start_date'));
        $to = date($request->input('end_date'));
        $points = $points->whereBetween('created_at', [$from, $to]);
    
    }
    elseif($request->has('start_date') && $request->input('start_date') != ''){
        $start_date = $request->input('start_date');
        $points=$points->whereDate('created_at', '>=', $start_date);
    }elseif($request->has('end_date') && $request->input('end_date') != ''){
        $end_date = $request->input('end_date');
        $points=$points->whereDate('created_at', '<=', $end_date );
    }

    $search='';
		if($request->has('search')){
            $search = $request->input('search');
        }

    if($search != ''){

        $points=$points->Where('refferal_code', 'like', '%' . $search . '%');
         
    }
   
        $points = $points->get()->ToArray();
    
    
    
    return view('frontend.customers.statements')->with(['login_info'=>$login_info,'points'=>$points]);

}

}

public function credits(Request $request){

    $login_info = auth()->guard('frontend')->user()->ToArray();

    $credits = RedCredits::where('fk_frontend_user_id',$login_info['id']);

    if($request->has('start_date') && $request->has('end_date') && $request->input('start_date') !='' && $request->input('end_date') != ''){

        $from = date($request->input('start_date'));
        $to = date($request->input('end_date'));
        $credits=$credits->whereBetween('created_at', [$from, $to]);
    
    }elseif($request->has('start_date') && $request->input('start_date') != ''){
        $start_date = $request->input('start_date');
        $credits=$credits->whereDate('created_at', '>=', $start_date);
    }elseif($request->has('end_date') && $request->input('end_date') != ''){
        $end_date = $request->input('end_date');
        $credits=$credits->whereDate('created_at', '<=', $end_date );
    }

    $search='';
		if($request->has('search')){
            $search = $request->input('search');
        }

    if($search != ''){

        $credits=$credits->Where('refferal_code', 'like', '%' . $search . '%');
         
    }
        
        $credits = $credits->get()->ToArray();
    
    
    //echo '<pre>';print_r($credits);exit;

    

    return view('frontend.customers.statements_credits')->with(['login_info'=>$login_info,'credits'=>$credits]);

}
    
}
