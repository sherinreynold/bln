<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Customers;
use App\Http\Controllers\Controller;
use App\Frontend;
use App\Models\RedGenies;
use App\Models\SalesGenies;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use URL;
use View;
use Auth;
use Session;
use Illuminate\Support\Str;
//use App\Frontend;
class AccountsController extends Controller
{
//
    public function forgot_password($value='')
    {
        return view('emails.forgot_password');
    }
    public function validatePasswordRequest(Request $request)
    {
        $user = DB::table('frontend_users')->where('email', '=', $request->email)
        ->first();
        if (count($user) < 1) {
            return redirect()->back()->withErrors(['email' => trans('Email ID does not exist')]);
        }
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
            ]);
        $tokenData = DB::table('password_resets')->where('email', $request->email)->first();
        $this->sendResetEmail($request->email, $tokenData->token);
        if ($this->sendResetEmail($request->email, $tokenData->token)=="true") {
            Session::flash('message', trans('A reset link has been sent to your email address.'));
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
        }
    }
    private function sendResetEmail($email, $token)
    {
        $user = DB::table('frontend_users')->where('email', $email)->select('email')->first();
        $link = 'password/reset/' . base64_encode($token);
        $link =URL::to($link);
        $mailData = array(
            'action'     => $link,
        );
        Mail::to($user->email)->send(new SendEmail($mailData));
        if (count(Mail::failures())<1) {
            return "true";
        }
    }
    public function showPasswordResetForm($token='')
    {
		$token=base64_decode($token);
        $tokenData = DB::table('password_resets')->where('token', $token)->first();
        if ( !$tokenData ) return redirect()->to('/'); 
        return View::make('emails.show')->with('tok', $token);
        exit;
    }
    public function resetPassword_submit(Request $request, $token)
    {

        $this->validate($request, [
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);



        $password = $request->password;
        $tokenData = DB::table('password_resets')->where('token', $token)->first();
        $user = Frontend::where('email', $tokenData->email)->first();
        if ( !$user ) return redirect()->to('/'); 
        $user->password = Hash::make($password);
        $user->update(); 
        Auth::login($user);
        DB::table('password_resets')->where('email', $user->email)->delete();
        Session::flash('message', trans('Please sign in with new password.'));
        return redirect()->to('/'); 
    }
}