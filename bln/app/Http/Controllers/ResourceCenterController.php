<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\ResourceCenters;


class ResourceCenterController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
        $rcs = DB::table('resource_center')->orderBy('created_at', 'desc')->paginate(10);
		
        //echo '<pre>';print_r($redgenies);exit;
        $breadcrumbs[0]['title']='Resource Centre';
		
        return view('dashboard.resource_centers.list', ['rcs' => $rcs,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $breadcrumbs[0]['title']='Resource Centre';
        $breadcrumbs[0]['link']=url('admin/resource_center');
        $breadcrumbs[1]['title']='Add';
        return view('dashboard.resource_centers.create',compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'title'           => 'required',
			'link'           => 'required'
        ]);
      
	   
	   
	   	 
			 
	    $rc=new ResourceCenters();
		$rc->title = $request->input('title');
		$rc->link = $request->input('link');
		$rc->type = $request->input('type');
        $rc->is_featured = $request->input('featured');
        $rc->resource_for = $request->input('r_for');
        $rc->status = $request->input('status');
		$rc->save();

	   
	   
        $request->session()->flash('message', ' Resources created Successfully');
        return redirect()->route('resource_center.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
		
		$rc = ResourceCenters::find($id);
        
        $breadcrumbs[0]['title']='Resource Centre';
        $breadcrumbs[0]['link']=url('admin/resource_center');
        $breadcrumbs[1]['title']='View';
		
        return view('dashboard.resource_centers.show', [ 'rc' => $rc,'breadcrumbs' => $breadcrumbs ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rc = ResourceCenters::find($id);
        //$rc = DB::table('resource_center')->where('id',$id)->get();
        
        $breadcrumbs[0]['title']='Resource Centre';
        $breadcrumbs[0]['link']=url('admin/resource_center');
        $breadcrumbs[1]['title']='Edit';
        
        return view('dashboard.resource_centers.edit', ['rc' => $rc,'breadcrumbs' => $breadcrumbs ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'title'           => 'required',
			'link'           => 'required'
        ]);
		
		
        $rc = ResourceCenters::find($id);
        
       
        $rc->title = $request->input('title');
		$rc->link = $request->input('link');
		$rc->type = $request->input('type');
        $rc->is_featured = $request->input('featured');
        $rc->resource_for = $request->input('r_for');
        $rc->status = $request->input('status');
		$rc->save();
		
		
		
		
        $request->session()->flash('message', 'Resources edited Successfully');
        return redirect()->route('resource_center.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rc = ResourceCenters::find($id);
		
        if($rc){
            
            $rc->delete();
        }
        
        
        return redirect()->route('resource_center.index');
    }
	
	
	
}
