<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesGenies;
use App\Models\RedGenies;
use App\Models\Customers;
use App\Models\RedPoints;

use App\Models\Frontend as Frontends;


use App\Frontend;

class NetworkController extends Controller
{

	public function index($value='')
	{
		
		$breadcrumbs[0]['title']='Network';
		return view('dashboard.network.tree', ['breadcrumbs' => $breadcrumbs]);
	}
	public function network_tree_view($value='')
	{
		# code...
		$data1=RedGenies::with('frontend')->get();
		foreach ($data1 as $row) {
			$text = $row->first_name;
			if (!$text) {
				$text = $row->frontend->email;
			}
		    $parent_id = $row->fk_ref_id;
		    $id = $row->frontend->id;
		    $type = "red_genies";

		    $text=$text.'<b> -Total amount:</b> $'.$this->getTotalPayments($id);
			if ($row->frontend) {
				$data[] = array('text' => $text, 'parent_id' => $parent_id, 'id' => $id , 'type' => $type);
			}
		}	

		$data2=SalesGenies::with('redgen')->get();
		foreach ($data2 as $row2) {
			$text = $row2->first_name;
			if (!$text) {
				$text = $row2->redgen->email;
			}
		    $parent_id = $row2->redgen->fk_frontend_user_id;
		    $id = $row2->fk_frontend_user_id;
		    $type = 'sales_genies';

		    $text=$text.'<b> -Total amount:</b> $'.$this->getTotalPayments($id);


			if ($row2->redgen) {
				$data[] = array('text' => $text, 'parent_id' => $parent_id, 'id' => $id , 'type' => $type);
			}
		}


		$data3=SalesGenies::with('salesgen')->get();
		foreach ($data3 as $row3) {
			$text = $row3->first_name;
			if (!$text) {
				$text = $row3->salesgen->email;
			}
			$parent_id = $row3->salesgen->fk_frontend_user_id;
			$id = $row3->fk_frontend_user_id;
			$type = 'sales_genies_plus';	

			$text=$text.'<b> -Total amount:</b> $'.$this->getTotalPayments($id);


			if ($row3->salesgen) {		    	
				$data[] = array('text' => $text, 'parent_id' => $parent_id, 'id' => $id , 'type' => $type);
			}
		}

		$data4=Customers::with('salesgen','frontend_customers')->get();
		foreach ($data4 as $row4) {
			$text = $row4->name;
			if (!$text) {
				$text = $row4->frontend_customers->email;
			}

			$parent_id = $row4->salesgen->fk_frontend_user_id;
			$id = $row4->fk_frontend_user_id;
			$type = 'file';
			$customer_code = $row4->code;

			$text=$text.'<b> -Total amount:</b> $'.$this->getCustomersTotalPayments($customer_code);
			
			if ($row4->salesgen) {
				$data[] = array('text' => $text, 'parent_id' => $parent_id, 'id' => $id , 'type' => $type);
			}
		}

		$itemsByReference = array();


		foreach($data as $key => &$item) {
		   $itemsByReference[$item['id']] = &$item;
		   // Children array:
		   $itemsByReference[$item['id']]['children'] = array();
		   $itemsByReference[$item['id']]['data'] = new \stdClass();
		}

		
		foreach($data as $key => &$item)
		   if($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
			  $itemsByReference [$item['parent_id']]['children'][] = &$item;

		
		foreach($data as $key => &$item) {
		   if($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
			  unset($data[$key]);
		}

		// Encode:
		echo json_encode(($data));exit;
		
	}


	public function getTotalPayments($frontend_id){

		$payments=0;
		
		$payments = RedPoints::where('fk_frontend_user_id',$frontend_id)->sum('invoice_amount');
	  
		return ($payments);
	  
	  }

	public function getCustomersTotalPayments($customer_code){
	    $payments = RedPoints::where('refferal_code',$customer_code)
	                ->groupBy('invoice_no')->get('invoice_amount')->toArray();
	    
	    $res=0;
	    if(!empty($payments)){
	             foreach($payments as $pay){
	                 $res=$res+$pay['invoice_amount'];
	             }
	    }
	      return($res);
	}  


}
