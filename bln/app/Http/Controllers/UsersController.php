<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\adminUserCreateSuccessEmail;
use App\User;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $you = auth()->user();
        $users = User::all();
        $breadcrumbs[0]['title']='Admin Users';
        return view('dashboard.admin.usersList', compact('users', 'you','breadcrumbs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $breadcrumbs[0]['title']='Admin Users';
        $breadcrumbs[0]['link']=url('admin/users');
        $breadcrumbs[1]['title']='View';
        return view('dashboard.admin.userShow', compact( 'user' ,'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $breadcrumbs[0]['title']='Admin Users';
        $breadcrumbs[0]['link']=url('admin/users');
        $breadcrumbs[1]['title']='Edit';
        return view('dashboard.admin.userEditForm', compact('user','breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'       => 'required|min:1|max:256',
            'email'      => 'required|email|max:256'
        ]);
        $user = User::find($id);
        $user->name       = $request->input('name');
        $user->email      = $request->input('email');
        $user->save();
        $request->session()->flash('message', 'Successfully updated user');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            $user->delete();
        }
        return redirect()->route('users.index');
    }


    public function create()
    {
        $breadcrumbs[0]['title']='Admin Users';
        $breadcrumbs[0]['link']=url('admin/users');
        $breadcrumbs[1]['title']='Add';
        return view('dashboard.admin.create',compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'f_name'           => 'required',
			'email_id'           => 'required'
        ]);
       // $user = auth()->user();
	   
	   
	   if (User::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('users.create')->withErrors('email id already exists');
             }
			 
			 
        $User=new User();
        $User->name = $request->input('f_name');
		$User->email = $request->input('email_id');
        $User->password = Hash::make($request->input('password'));
        $User->menuroles = 'user';
		$User->save();

	

        $mailData = array(
            'name'     => $request->input('f_name'),
            'username'     => $request->input('email_id'),
            'password'     => $request->input('password'),
        );
        Mail::to($request->input('email_id'))->send(new adminUserCreateSuccessEmail($mailData));

        $request->session()->flash('message', 'Successfully created User and credential sent to '.$request->input('email_id'));
        return redirect()->route('users.index');
    }


}
