<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvitationEmail;
use App\Mail\adminCreateSuccessEmail;
use App\Models\SalesGenies;
use App\Models\RedGenies;
use App\Models\Customers;
use App\Frontend;

class SalesGenieController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$type_filter='';
		if($request->has('type')){
            $type_filter = $request->input('type');
        }
        $code_filter='';
		if($request->has('code')){
            $code_filter = $request->input('code');
        }



		$salesgenies = DB::table('frontend_users')->whereIn('bln_role_id', [ '2', '3']);
        
        if($type_filter != ''){
            //echo $type_filter;
			$salesgenies = $salesgenies->where('bln_role_id', $type_filter);
        }



        if($code_filter != ''){
            $salesgenies=$salesgenies->where('code','like','%'.$code_filter.'%'); 
        }
        
        $salesgenies=$salesgenies->select('*')->orderBy('created_at', 'desc')->paginate(10);
        
        $breadcrumbs[0]['title']='Sales Genie';
        return view('dashboard.salesgenies.salesgeniesList', ['notes' => $salesgenies,'type_filter' => $type_filter,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $redgenies = Frontend::all(['name','id','code','bln_role_id'])->where('bln_role_id','1')->sortBy("name")->toArray();
       $salesgenie_plus = Frontend::where('bln_role_id','2')->get(['name','id','code','bln_role_id'])
       ->sortBy("name")->toArray();
        //echo '<pre>';print_r($redgenies);exit;

        $breadcrumbs[0]['title']='Sales Genie';
        $breadcrumbs[0]['link']=url('admin/sales_genie');
        $breadcrumbs[1]['title']='Add';
	   return view('dashboard.salesgenies.create', [ 'redgenies' => $redgenies, 'salesgenie_plus' => $salesgenie_plus ,'breadcrumbs' => $breadcrumbs ]);
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			'mobile'           => 'required',
			//'redgenie_id'      => 'required',
			'type'             => 'required'
        ]);
       // $user = auth()->user();

      
       /* parent limit check  */

       if($request->input('type')=='1'){
        $role_id='3';   
        $pid = $request->input('salesgenieplus_id');
        $p_limit=Frontend::where('parent_id',$pid)
                              ->where('bln_role_id','3')->count();
        }else{
        $role_id='2';    
        $pid = $request->input('redgenie_id'); 
        $p_limit=Frontend::where('parent_id',$pid)
                             ->where('bln_role_id','2')->count();  
        }
        if ($p_limit >=5) {
            return redirect()->route('sales_genie.create')->withErrors('This parent has exceed the limit');
               }
       
       /* parent limit check ends */

	   
	   if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('sales_genie.create')->withErrors('email id already exists');
             }
			 
	   if (Frontend::where('username', '=', $request->input('username'))->exists()) {
          return redirect()->route('sales_genie.create')->withErrors('username already exists');
             }		 
	   
	   
	    $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->username = $request->input('username');
        $frontend_user->bln_role_id = $role_id;
        $frontend_user->parent_id = $pid;
		$frontend_user->password = Hash::make($request->input('password'));
		$frontend_user->save();
		
		
		$code=str_pad($frontend_user->id,8, '0', STR_PAD_LEFT);
	    $user_code="BLN".$code;
	   
        $note = Frontend::find($frontend_user->id);
		$note->code = $user_code;
		
        $note->name     = $request->input('f_name');
		$note->phone = $request->input('mobile');
        $note->address = $request->input('address');
       /* $note->city = $request->input('city');
		$note->state = $request->input('state');
		$note->zip = $request->input('zip');*/

       
		
        
        $note->save();

        $mailData = array(
            'name'     => $request->input('f_name'),
            'username'     => $request->input('username'),
            'password'     => $request->input('password'),
        );
       // Mail::to($request->input('email_id'))->send(new adminCreateSuccessEmail($mailData));

        $request->session()->flash('message', 'Successfully created Sales Genie and credential sent to '.$request->input('email_id'));
        return redirect()->route('sales_genie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $info = DB::table('frontend_users')->select(['frontend_users.*','parent.name as parent_name','parent.code as parent_code','role.role'])
        ->leftjoin('frontend_users as parent', 'frontend_users.parent_id', '=', 'parent.id')
        ->leftjoin('bln_roles as role', 'frontend_users.bln_role_id', '=', 'role.id')
        ->where('frontend_users.id',$id)->get()->toArray();
            
        //echo '<pre>';print_r($info);exit;
        $breadcrumbs[0]['title']='Sales Genie';
        $breadcrumbs[0]['link']=url('admin/sales_genie');
        $breadcrumbs[1]['title']='View';
	    return view('dashboard.salesgenies.show', [ 'note' => $info,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*$info = DB::table('sales_genies')->select(['sales_genies.*','red_genies.first_name as rf_name','red_genies.last_name as rl_name','fu.email','sg.first_name as sg_name','sg.code as sg_code'])
        ->leftjoin('red_genies', 'red_genies.id', '=', 'sales_genies.fk_red_genie_id')
        ->join('frontend_users as fu', 'sales_genies.fk_frontend_user_id', '=', 'fu.id')
        ->leftjoin('sales_genies as sg', 'sg.id', '=', 'sales_genies.fk_sales_genie_id')
        ->where('sales_genies.id',$id)->get()->toArray();*/
        //echo '<pre>';print_r($info);exit;
        
        $info = DB::table('frontend_users')->select(['frontend_users.*','parent.id as parent_user_id','parent.name as parent_name','parent.code as parent_code','role.role'])
        ->leftjoin('frontend_users as parent', 'frontend_users.parent_id', '=', 'parent.id')
        ->leftjoin('bln_roles as role', 'frontend_users.bln_role_id', '=', 'role.id')
        ->where('frontend_users.id',$id)->get()->toArray();


        $redgenies = Frontend::where('bln_role_id','1')->get(['name','id','code'])
        ->sortBy("first_name")->toArray();
        
        $salesgenie_plus = Frontend::where('bln_role_id','2')->get(['name','id','code'])
       ->sortBy("first_name")->toArray();

       //echo '<pre>';print_r($info);exit;

       $breadcrumbs[0]['title']='Sales Genie';
       $breadcrumbs[0]['link']=url('admin/sales_genie');
        $breadcrumbs[1]['title']='Edit';
	    return view('dashboard.salesgenies.edit', ['note' => $info[0],'redgenies' => $redgenies,'salesgenie_plus' => $salesgenie_plus ,'breadcrumbs'=>$breadcrumbs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			'email_id'           => 'required',
			'mobile'           => 'required',
			//'redgenie_id'      => 'required',
			'type'             => 'required'
        ]);
		
		
        $note = Frontend::find($id);
        
		
		 
       
        $note->name     = $request->input('f_name');
        //$note->last_name   = $request->input('l_name');
		$note->phone = $request->input('mobile');
        $note->address = $request->input('address');
        $note->email = $request->input('email_id');
       /* $note->city = $request->input('city');
		$note->state = $request->input('state');
		$note->zip = $request->input('zip');*/
        //$note->fk_red_genie_id = $request->input('redgenie_id');
        
        if($request->input('type')=='1'){
            $note->parent_id = $request->input('salesgenieplus_id');
            $role_id='3';    
            
            }else{
            $note->parent_id = $request->input('redgenie_id');
            $role_id='2';  
            }

		$note->bln_role_id = $role_id;
		
		
        $note->save();
		
		
        $request->session()->flash('message', 'Successfully edited Sales Genie');
        return redirect()->route('sales_genie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Frontend::where('parent_id', '=', $id)->exists()) {
            return redirect()->route('sales_genie.index')->withErrors("Can't delete this user.Some sales genies/customers are registered under this sales genie+");
               }

           
            DB::table('frontend_users')->delete($id);
        
        
		
        return redirect()->route('sales_genie.index');
    }
	
	
	//reg links
	
	public function regLinks($salesgenie_id)
    {
       
		//$customer_links =	 Customers::where('ref_id', '=', $salesgenie_id)->paginate(10);
          $ref_info=SalesGenies::where('id', '=', $salesgenie_id)->get()->ToArray();
          $customer_links =  DB::table('customer as c')->join('frontend_users as fu', 'c.fk_frontend_user_id', '=', 'fu.id')->select('c.*','fu.email')->where('c.ref_code',$ref_info[0]['code'])->paginate(10);		
		
        //print_r($customer_links);exit;
        
        $breadcrumbs[0]['title']='Sales Genie';
        $breadcrumbs[0]['link']=url('admin/sales_genie');
        $breadcrumbs[1]['title']='Registration Links';
		
        return view('dashboard.salesgenies.regLinks',['customer_links' => $customer_links,'salesgenie_id' => $salesgenie_id,'breadcrumbs' => $breadcrumbs]);
    }
	
	public function addRegLinks($salesgenie_id){

        $breadcrumbs[0]['title']='Sales Genie';
        $breadcrumbs[0]['link']=url('admin/sales_genie');
        $breadcrumbs[1]['title']='Registration Links';
        $breadcrumbs[1]['link']=url('admin/sales_genie/reg_links/'.$salesgenie_id);
        $breadcrumbs[2]['title']='Add';
		
		return view('dashboard.salesgenies.add_reg_links',['salesgenie_id' => $salesgenie_id,'breadcrumbs'=>$breadcrumbs]);
		
	}
	
	public function saveRegLinks($salesgenie_id,Request $request){
		
		 $validatedData = $request->validate([
            
            'name'           => 'required',
			
			'email_id'           => 'required'
        ]);
       
	   	    
			 
		 if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('sales_genie.reg_links',[$salesgenie_id])->withErrors('email id already exists');
             }
			 

        $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->user_type = 'customer';
		$frontend_user->save();			 
	   
	     
		$code=str_pad($frontend_user->id,10, '0', STR_PAD_LEFT);
	    $user_code="C".$code; 
		 
        $c = new Customers();
		$c->code = $user_code;
		$c->fk_frontend_user_id = $frontend_user->id;
        $c->name     = $request->input('name');
		$c->mobile = $request->input('mobile');
		$c->type = $request->input('type');
		
        //$c->ref_by = 2;
        $ref_info=SalesGenies::where('id', '=', $salesgenie_id)->get()->ToArray();

		$c->ref_code = $ref_info[0]['code'];
       
        
        $c->save();

        $mailData = array(
            'name'     => $request->input('name'),
            'action'     => url('registration/'.base64_encode($user_code)),
        );
        Mail::to($request->input('email_id'))->send(new InvitationEmail($mailData));

        $request->session()->flash('message', 'Registration link sent to '.$request->input('email_id'));
        
		return redirect()->route('sales_genie.reg_links', [$salesgenie_id]);
    }
    

    public function deleteRegLinks($id,$salesgenie_id,Request $request)
    {

       
       
        $c = Customers::find($id);
		
        if($c){
            DB::table('frontend_users')->delete($c->fk_frontend_user_id);
            $c->delete();
        }
        
        $request->session()->flash('message', 'Registration link deleted successfully');
        return redirect()->route('sales_genie.reg_links',[$salesgenie_id]);
    }
}
