<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RedGenies;
use App\Models\SalesGenies;
use App\Models\Customers;
use App\Models\RedPoints;
use App\Frontend;

class PointController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';
		if($request->has('search')){
            $search = $request->input('search');
        }
		
        $points = RedPoints::leftjoin('red_genies', 'red_genies.fk_frontend_user_id', '=', 'red_points.fk_frontend_user_id')
        ->leftjoin('sales_genies as sg', 'sg.fk_frontend_user_id', '=', 'red_points.fk_frontend_user_id');
        
        if($request->has('start_date') && $request->has('end_date') && $request->input('start_date') !='' && $request->input('end_date') != ''){
            $points=$points->whereBetween('created_at', [$request->input('start_date'), $request->input('end_date')]);
        }
		elseif($request->has('start_date') && $request->input('start_date') != ''){
            $start_date = $request->input('start_date');
            $points=$points->whereDate('created_at', '>=', $start_date);
        }elseif($request->has('end_date') && $request->input('end_date') != ''){
            $end_date = $request->input('end_date');
            $points=$points->whereDate('created_at', '<=', $end_date );
        }

        if($search != ''){

            $points=$points->Where('refferal_code', 'like', '%' . $search . '%');
                             // ->orWhere('red_genies.code', 'like', '%' . $search . '%')
                             //->orWhere('sg.code', 'like', '%' . $search . '%');

        }

        $points=$points->select(['red_points.*','sg.code as sg_code','red_genies.code as rg_code'])
                       ->selectRaw('GROUP_CONCAT(red_genies.code SEPARATOR "  ") as rg_codes')
                       ->selectRaw('GROUP_CONCAT(sg.code SEPARATOR "  ") as sg_codes')
                       ->groupBy('red_points.invoice_no')
                       ->paginate(10);
        //echo '<pre>';print_r($points->toArray());exit;
        $breadcrumbs[0]['title']='Red Points';
		
        return view('dashboard.points.list',compact('breadcrumbs','points'));
    }



    public function create()
    {
       $redgenies = RedGenies::all(['first_name','last_name','id','code','fk_frontend_user_id'])->sortBy("first_name")->toArray();
       $salesgenie_plus = SalesGenies::all(['first_name','last_name','id','code','fk_frontend_user_id'])
       ->sortBy("first_name")->toArray();
        //echo '<pre>';print_r($salesgenie_plus);exit;

        $breadcrumbs[0]['title']='Red Points';
        $breadcrumbs[0]['link']=url('admin/points');
        $breadcrumbs[1]['title']='Add';
	   return view('dashboard.points.create', [ 'redgenies' => $redgenies, 'salesgenie_plus' => $salesgenie_plus ,'breadcrumbs' => $breadcrumbs ]);
   }


   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'point'           => 'required',
			
        ]);
            

        $red_points=new RedPoints();

        if($request->input('parent') == 's'){
            $red_points->fk_frontend_user_id = $request->input('salesgenieplus_id'); //salesgenie user id
        }else{
            $red_points->fk_frontend_user_id = $request->input('redgenie_id'); 
        }
	   
        $red_points->point = $request->input('point');
        $red_points->refferal_code = 'admin';
        $red_points->save();
        
        $invoice_no='adm'.$red_points->id;
        $p = RedPoints::find($red_points->id);
        $p->invoice_no=$invoice_no;
        $p->save();
		
		
		

        $request->session()->flash('message', 'Points addedd Successfully');
        return redirect()->route('points.index');
    }

    
    public function view($id)
    {
        $credits = RedPoints::leftjoin('red_genies', 'red_genies.fk_frontend_user_id', '=', 'red_points.fk_frontend_user_id')
        ->leftjoin('sales_genies as sg', 'sg.fk_frontend_user_id', '=', 'red_points.fk_frontend_user_id')
        ->where('red_points.invoice_no',$id)
        ->select(['red_points.*','sg.code as sg_code','red_genies.code as rg_code'])->get();
        //echo '<pre>';print_r($credits->toArray());exit;
		
        $breadcrumbs[0]['title']='Red Points';
        $breadcrumbs[0]['link']=url('admin/points');
        $breadcrumbs[1]['title']='Details';
		
        return view('dashboard.points.show',compact('breadcrumbs','credits'));
    }



    public function edit($id)
    {
        $redgenies = RedGenies::all(['first_name','last_name','id','code','fk_frontend_user_id'])->sortBy("first_name")->toArray();
        $salesgenie_plus = SalesGenies::all(['first_name','last_name','id','code','fk_frontend_user_id'])
        ->sortBy("first_name")->toArray();


        $note = RedPoints::where('red_points.id',$id)
                ->join('frontend_users as f','f.id','=','red_points.fk_frontend_user_id')
                ->get(['red_points.*','f.user_type']);	
        //echo '<pre>';print_r($note);exit;
        $breadcrumbs[0]['title']='Red Points';
        $breadcrumbs[0]['link']=url('admin/points');
        $breadcrumbs[1]['title']='Edit';
        return view('dashboard.points.edit', ['note' => $note[0] ,'breadcrumbs' => $breadcrumbs, 'redgenies' => $redgenies, 'salesgenie_plus' => $salesgenie_plus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'point'           => 'required',
			
        ]);
		
		
        
        
        $red_points=RedPoints::find($id);

        if($request->input('parent') == 's'){
            $red_points->fk_frontend_user_id = $request->input('salesgenieplus_id'); //salesgenie user id
        }else{
            $red_points->fk_frontend_user_id = $request->input('redgenie_id'); 
        }
	   
        $red_points->point = $request->input('point');
        //$red_points->refferal_code = 'admin';
        $red_points->save();
		
		
		

        $request->session()->flash('message', 'Points updated Successfully');
        return redirect()->route('points.index');
    }

   
	
	
	
	
}
