<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\adminCreateSuccessEmail;
use App\Models\Customers;
use App\Frontend;

class RedGenieController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $code_filter='';
		if($request->has('code')){
            $code_filter = $request->input('code');
        }
        
        if($code_filter != ''){

            $redgenies = DB::table('frontend_users')
                        ->where('code','like','%'.$code_filter.'%') 
                        ->where('bln_role_id','1')  
                        ->select('*')->orderBy('created_at', 'desc')->paginate(10);

        }else{
           $redgenies = DB::table('frontend_users')->select('*')
                        ->where('bln_role_id','1')
                        ->orderBy('created_at', 'desc')->paginate(10);
        }
       // echo '<pre>';print_r($redgenies);exit;
        $breadcrumbs[0]['title']='Red Genie';
		
        return view('dashboard.redgenies.redgeniesList', ['notes' => $redgenies,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo $code=str_pad(111,8, '0', STR_PAD_LEFT);exit;
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='Add';
        return view('dashboard.redgenies.create',compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			'email_id'           => 'required',
			'mobile'           => 'required'
        ]);
       // $user = auth()->user();
	   
	   
	   if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('red_genie.create')->withErrors('email id already exists');
             }
			 
	   if (Frontend::where('username', '=', $request->input('username'))->exists()) {
          return redirect()->route('red_genie.create')->withErrors('username already exists');
             }		 
			 
	    $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->username = $request->input('username');
        $frontend_user->bln_role_id = '1';
        $frontend_user->parent_id=1;
		$frontend_user->password = Hash::make($request->input('password'));
		$frontend_user->save();

	   
	    $code=str_pad($frontend_user->id,8, '0', STR_PAD_LEFT);
	    $user_code="BLN".$code; 
	   
	   
        $note = Frontend::find($frontend_user->id);
		$note->code = $user_code;
        $note->name     = $request->input('f_name');
        //$note->last_name   = $request->input('l_name');
		$note->phone = $request->input('mobile');
        $note->address = $request->input('address');
        
        
        $note->save();

        $mailData = array(
            'name'     => $request->input('f_name'),
            'username'     => $request->input('username'),
            'password'     => $request->input('password'),
        );
        //mail sent hided
        //Mail::to($request->input('email_id'))->send(new adminCreateSuccessEmail($mailData));

        $request->session()->flash('message', 'Successfully created Red Genie and credential sent to '.$request->input('email_id'));
        return redirect()->route('red_genie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$note = RedGenies::all()->find($id);
		
		$note = DB::table('frontend_users')->select('*')->where('id',$id)->get();
		
		//echo '<pre>';print_r($note);exit;
		
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='View';
		
        return view('dashboard.redgenies.show', [ 'note' => $note[0],'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$note = RedGenies::find($id);
		$note = DB::table('frontend_users')->select('*')->where('id',$id)->get();
				
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='Edit';
        return view('dashboard.redgenies.edit', ['note' => $note[0] ,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			//'email_id'           => 'required',
			'mobile'           => 'required'
        ]);
		
		
        $note = Frontend::find($id);
        
       
        $note->name     = $request->input('f_name');
		$note->phone = $request->input('mobile');
        $note->address = $request->input('address');
        //$note1->email = $request->input('email_id');
        /*$note->city = $request->input('city');
		$note->state = $request->input('state');
		$note->zip = $request->input('zip');*/
		
		$note->save();
		
				
		
		
		
		
        $request->session()->flash('message', 'Successfully edited Red Genie');
        return redirect()->route('red_genie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		
		if (Frontend::where('parent_id', '=', $id)->exists()) {
          return redirect()->route('red_genie.index')->withErrors("Can't delete this user.Some sales genies+ / customers are registered under this red genie");
             }
		
       
            DB::table('frontend_users')->delete($id);
           
		
        return redirect()->route('red_genie.index');
    }
	
	
	
	//reg links
	
	public function regLinks($redgenie_id)
    {
        /*$customer_links = DB::table('customer')
                     ->select(DB::raw('*'))
                     ->where('ref_id', '=', $redgenie_id)
                     ->get();*/
					 
		$customer_links =	 Customers::where('ref_id', '=', $redgenie_id)->paginate(10);
		
		//print_r($customer_links);exit;
		
        return view('dashboard.redgenies.regLinks',['customer_links' => $customer_links,'redgenie_id' => $redgenie_id]);
    }
	
	public function addRegLinks($redgenie_id){
		
		return view('dashboard.redgenies.add_reg_links',['redgenie_id' => $redgenie_id]);
		
	}
	
	public function saveRegLinks($redgenie_id,Request $request){
		
		 $validatedData = $request->validate([
            
            'name'           => 'required',
			
			'email_id'           => 'required'
        ]);
       // $user = auth()->user();
	   
	    if (Customers::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('red_genie.reg_links',[$redgenie_id])->withErrors('email id already exists');
             }
	   
	   
        $c = new Customers();
        $c->name     = $request->input('name');
       
        $c->email = $request->input('email_id');
		$c->mobile = $request->input('mobile');
		$c->type = $request->input('type');
		
		$c->ref_by = 1;
		$c->ref_id = $redgenie_id;
       
        
        $c->save();
        $request->session()->flash('message', 'Successfully created Registration link');
        
		return redirect()->route('red_genie.reg_links', [$redgenie_id]);
	}
}
