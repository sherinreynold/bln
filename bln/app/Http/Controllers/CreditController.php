<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\RedGenies;
use App\Models\SalesGenies;
use App\Models\Customers;
use App\Models\RedCredits;
use App\Frontend;

class CreditController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';
		if($request->has('search')){
            $search = $request->input('search');
        }

        
		
        $credits = RedCredits::leftjoin('red_genies', 'red_genies.fk_frontend_user_id', '=', 'red_credits.fk_frontend_user_id')
        ->leftjoin('sales_genies as sg', 'sg.fk_frontend_user_id', '=', 'red_credits.fk_frontend_user_id');

        

        if($request->has('start_date') && $request->has('end_date') && $request->input('start_date') !='' && $request->input('end_date') != ''){
            $credits=$credits->whereBetween('created_at', [$request->input('start_date'), $request->input('end_date')]);
        }
		elseif($request->has('start_date') && $request->input('start_date') != ''){
            $start_date = $request->input('start_date');
            $credits=$credits->whereDate('created_at', '>=', $start_date);
        }elseif($request->has('end_date') && $request->input('end_date') != ''){
            $end_date = $request->input('end_date');
            $credits=$credits->whereDate('created_at', '<=', $end_date );
        }

        if($search != ''){

            $credits=$credits->Where('refferal_code', 'like', '%' . $search . '%');
                             // ->orWhere('red_genies.code', 'like', '%' . $search . '%')
                             //->orWhere('sg.code', 'like', '%' . $search . '%');

        }


        

        $credits=$credits->select(['red_credits.*','sg.code as sg_code','red_genies.code as rg_code'])->paginate(10);
       // echo '<pre>';print_r($credits->toArray());exit;
        $breadcrumbs[0]['title']='Red Credits';
		
        return view('dashboard.credits.list',compact('breadcrumbs','credits'));
    }

    
    public function view($id)
    {
        $credits = RedCredits::leftjoin('red_genies', 'red_genies.fk_frontend_user_id', '=', 'red_credits.fk_frontend_user_id')
        ->leftjoin('sales_genies as sg', 'sg.fk_frontend_user_id', '=', 'red_credits.fk_frontend_user_id')
        ->where('red_credits.refferal_code',$id)
        ->select(['red_credits.*','sg.code as sg_code','red_genies.code as rg_code'])->get();
        //echo '<pre>';print_r($credits->toArray());exit;
		
        $breadcrumbs[0]['title']='Red Credits';
        $breadcrumbs[0]['link']=url('admin/credits');
        $breadcrumbs[1]['title']='Details';
		
        return view('dashboard.credits.show',compact('breadcrumbs','credits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$note = RedGenies::find($id);
		$note = DB::table('red_genies as r')->join('frontend_users as fu', 'r.fk_frontend_user_id', '=', 'fu.id')->select('r.*','fu.email')->where('r.id',$id)->get();
				
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='Edit';
        return view('dashboard.redgenies.edit', ['note' => $note[0] ,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			'email_id'           => 'required',
			'mobile'           => 'required'
        ]);
		
		
        $note = RedGenies::find($id);
        
       
        $note->first_name     = $request->input('f_name');
        //$note->last_name   = $request->input('l_name');
        //$note->email = $request->input('email_id');
		$note->mobile = $request->input('mobile');
        $note->address = $request->input('address');
        $note->city = $request->input('city');
		$note->state = $request->input('state');
		$note->zip = $request->input('zip');
		
		$note->save();
		
		
		$note1 = Frontend::find($note->fk_frontend_user_id);
		
		$note1->email = $request->input('email_id');
		$note1->save();
		
		
		
		
        $request->session()->flash('message', 'Successfully edited Red Genie');
        return redirect()->route('red_genie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		
		if (SalesGenies::where('fk_red_genie_id', '=', $id)->exists()) {
          return redirect()->route('red_genie.index')->withErrors("Can't delete. This Redgenie have assigned SalesGenies");
             }
		
        $red = RedGenies::find($id);
		
        if($red){
            DB::table('frontend_users')->delete($red->fk_frontend_user_id);
            $red->delete();
        }
		
        return redirect()->route('red_genie.index');
    }
	
	
	
	
}
