<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\adminCreateSuccessEmail;
use App\Models\RedGenies;
use App\Models\Customers;
use App\Models\SalesGenies;
use App\Frontend;

class CustomerController extends Controller
{
    
    
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $code_filter='';
        $reff_code_filter='';

		if($request->has('code')){
            $code_filter = $request->input('code');
        }

        if($request->has('reff_code')){
            $reff_code_filter = $request->input('reff_code');
        }
        
        $customers = DB::table('customer as c')->join('frontend_users as fu', 'c.fk_frontend_user_id', '=', 'fu.id');
        if($code_filter != ''){
            $customers=  $customers->where('c.code','like','%'.$code_filter.'%');  
                        
        }
        if($reff_code_filter != ''){
            $customers=  $customers->where('c.ref_code','like','%'.$reff_code_filter.'%');  
                        
        }


           $customers = $customers->select('c.*','fu.email')
                        ->orderBy('created_at', 'desc')->paginate(10);
        
        //echo '<pre>';print_r($redgenies);exit;
        $breadcrumbs[0]['title']='Customers';
		
        return view('dashboard.customers.list', ['notes' => $customers,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='Add';
        return view('dashboard.redgenies.create',compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			'email_id'           => 'required',
			'mobile'           => 'required'
        ]);
       // $user = auth()->user();
	   
	   
	   if (Frontend::where('email', '=', $request->input('email_id'))->exists()) {
          return redirect()->route('red_genie.create')->withErrors('email id already exists');
             }
			 
	   if (Frontend::where('username', '=', $request->input('username'))->exists()) {
          return redirect()->route('red_genie.create')->withErrors('username already exists');
             }		 
			 
	    $frontend_user=new Frontend();
		$frontend_user->email = $request->input('email_id');
		$frontend_user->username = $request->input('username');
		$frontend_user->user_type = 'redgenie';
		$frontend_user->password = Hash::make($request->input('password'));
		$frontend_user->save();

	   
	    $code=str_pad($frontend_user->id,10, '0', STR_PAD_LEFT);
	    $user_code="RG".$code; 
	   
	   
        $note = new RedGenies();
		$note->code = $user_code;
		$note->fk_frontend_user_id = $frontend_user->id;
        $note->first_name     = $request->input('f_name');
        //$note->last_name   = $request->input('l_name');
		$note->mobile = $request->input('mobile');
        $note->address = $request->input('address');
        $note->city = $request->input('city');
		$note->state = $request->input('state');
		$note->zip = $request->input('zip');
        
        $note->save();

        $mailData = array(
            'name'     => $request->input('f_name'),
            'username'     => $request->input('username'),
            'password'     => $request->input('password'),
        );
        Mail::to($request->input('email_id'))->send(new adminCreateSuccessEmail($mailData));

        $request->session()->flash('message', 'Successfully created Red Genie and credential sent to '.$request->input('email_id'));
        return redirect()->route('red_genie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$note = RedGenies::all()->find($id);
		
		$note = DB::table('red_genies as r')->join('frontend_users as fu', 'r.fk_frontend_user_id', '=', 'fu.id')->select('r.*','fu.email')->where('r.id',$id)->get();
		
		//echo '<pre>';print_r($note);exit;
		
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='View';
		
        return view('dashboard.redgenies.show', [ 'note' => $note[0],'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$note = RedGenies::find($id);
		$note = DB::table('red_genies as r')->join('frontend_users as fu', 'r.fk_frontend_user_id', '=', 'fu.id')->select('r.*','fu.email')->where('r.id',$id)->get();
				
        $breadcrumbs[0]['title']='Red Genie';
        $breadcrumbs[0]['link']=url('admin/red_genie');
        $breadcrumbs[1]['title']='Edit';
        return view('dashboard.redgenies.edit', ['note' => $note[0] ,'breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            
            'f_name'           => 'required',
			//'l_name'           => 'required',
			'email_id'           => 'required',
			'mobile'           => 'required'
        ]);
		
		
        $note = RedGenies::find($id);
        
       
        $note->first_name     = $request->input('f_name');
        //$note->last_name   = $request->input('l_name');
        //$note->email = $request->input('email_id');
		$note->mobile = $request->input('mobile');
        $note->address = $request->input('address');
        $note->city = $request->input('city');
		$note->state = $request->input('state');
		$note->zip = $request->input('zip');
		
		$note->save();
		
		
		$note1 = Frontend::find($note->fk_frontend_user_id);
		
		$note1->email = $request->input('email_id');
		$note1->save();
		
		
		
		
        $request->session()->flash('message', 'Successfully edited Red Genie');
        return redirect()->route('red_genie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		
		if (SalesGenies::where('fk_red_genie_id', '=', $id)->exists()) {
          return redirect()->route('red_genie.index')->withErrors("Can't delete this user.Some sales genies+ are registered under this red genie");
             }
		
        $red = RedGenies::find($id);
		
        if($red){
            DB::table('frontend_users')->delete($red->fk_frontend_user_id);
            $red->delete();
        }
		
        return redirect()->route('red_genie.index');
    }
	
	
	
	
}
