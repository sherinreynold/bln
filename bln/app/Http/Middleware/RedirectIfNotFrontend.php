<?php

namespace App\Http\Middleware;
use Closure;
class RedirectIfNotFrontend
{
    
    public function handle($request, Closure $next, $guard="frontend")
    {
        if(!auth()->guard($guard)->check()) {
            return redirect(route('frontend.login'));
        }
        return $next($request);
    }
}
