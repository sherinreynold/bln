<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RedGenies extends Model
{
    protected $table = 'red_genies';
	public $timestamps = false;
	
	public function frontend()
	{
	    return $this->belongsTo('App\Models\Frontend', 'fk_frontend_user_id', 'id');
	}
	
	public function scopeFilter($query, $request)
    {
    	if ($request['id']) {
            $query->where('id', '=', $request['id']);
        }else{
        	$query->where('fk_frontend_user_id', '=', $request['fk_frontend_user_id']);
        }
        return $query;
    }
	
	
}
