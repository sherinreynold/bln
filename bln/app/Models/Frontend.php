<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Frontend extends Model
{

   
   protected $table = 'frontend_users';
 
    protected $fillable = [
        'email', 'password', 'api_token'
    ];
 
    protected $hidden = [
        'password'
    ];
}
