<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class SalesGenies extends Model
{
    protected $table = 'sales_genies';
	public $timestamps = false;
	
	public function redgen()
	{
	    return $this->belongsTo('App\Models\RedGenies', 'fk_red_genie_id', 'id');
	}
	
	public function salesgen()
	{
	    return $this->belongsTo('App\Models\SalesGenies', 'fk_sales_genie_id', 'id');
	}
	
	public function scopeFilter($query, $request, $type=null)
    {

    	if ($request['id']) {
            $query->where('id', '=', $request['id']);
        }
		
    	if ($request['fk_frontend_user_id'] && $type==null) {
            $query->where('fk_frontend_user_id', '=', $request['fk_frontend_user_id']);
        }
		
		if ($request['fk_frontend_user_id'] && $type=='1') {
            $query->where('fk_frontend_user_id', '=', $request['fk_frontend_user_id'])->where('type', '=', '1');
        }

    	if ($request['fk_frontend_user_id'] && $type=='2') {
            $query->where('fk_frontend_user_id', '=', $request['fk_frontend_user_id'])->where('type', '=', '2');
        }
		
        if ($request['fk_red_genie_id'] && $type==null) {
            $query->where('fk_red_genie_id', '=', $request['fk_red_genie_id']);
        }
		
        if ($request['fk_red_genie_id'] && $type=='2') {
          $query->where('fk_red_genie_id', '=', $request['fk_red_genie_id'])->where('type', '=', '2');
        }


        if ($request['fk_red_genie_id'] && $type=='1') {
          $query->where('fk_red_genie_id', '=', $request['fk_red_genie_id'])->where('type', '=', '1');
        }
        if ($request['fk_sales_genie_id'] && $type==null) {
            $query->where('fk_sales_genie_id', '=', $request['fk_sales_genie_id']);
        }
        if ($request['fk_sales_genie_id'] && $type=='1') {
            $query->where('fk_sales_genie_id', '=', $request['fk_sales_genie_id'])->where('type', '=', '1');
        }
        return $query;
    }
}

