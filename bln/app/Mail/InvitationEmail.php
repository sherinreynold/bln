<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
class InvitationEmail extends Mailable
{
    use Queueable, SerializesModels;
/**
* Create a new message instance.
*
* @return void
*/
public $mailData;
public function __construct($mailData)
{
    $this->mailData = $mailData;
}
/**
* Build the message.
*
* @return $this
*/
public function build()
{
    $input = array(
        'name'     => $this->mailData['name'],
        'action'     => $this->mailData['action'],
        );
    return $this->from('admin@programmingfields.com')->subject('Refer To Gain')->view('emails.ivitation_mail')->with(['inputs' => $input ]);
}
}
