-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2020 at 06:29 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_r2g`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `fk_frontend_user_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `mobile` varchar(150) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1:customer/vendor\r\n2:employee',
  `ref_by` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `example`
--

CREATE TABLE `example` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `example`
--

INSERT INTO `example` (`id`, `created_at`, `updated_at`, `name`, `description`, `status_id`) VALUES
(1, NULL, NULL, 'Voluptates aut ipsam expedita voluptatem at.', 'Eveniet et et aut quaerat facilis. Recusandae voluptatem veritatis aut ab inventore accusamus. Officia vel accusamus cupiditate aliquam minima eveniet et ut. Omnis quasi ex et maxime ex.', 2),
(2, NULL, NULL, 'Rerum nisi nemo excepturi.', 'Sit possimus est pariatur enim consequatur id. Accusantium nobis dolore consequatur qui ut qui. Animi perferendis neque eveniet in reiciendis consequatur.', 2),
(3, NULL, NULL, 'Minus commodi porro dolores sed.', 'Porro aut excepturi non in. Provident impedit culpa tenetur tempore non perferendis. Et dolor necessitatibus temporibus totam dignissimos et sit quidem.', 2),
(4, NULL, NULL, 'Doloribus cum provident et.', 'Et reprehenderit illo qui excepturi et ipsam. Facilis cupiditate nihil illo quibusdam aut. Facilis sit aut dolor autem mollitia ipsam et. Rerum voluptas excepturi quibusdam veritatis blanditiis deleniti velit.', 3),
(5, NULL, NULL, 'Est odio voluptates laborum.', 'Voluptatem consequuntur ut itaque saepe aperiam ratione. Quo neque possimus excepturi occaecati placeat voluptatem voluptates. Earum amet maxime quam eos est facilis.', 3),
(6, NULL, NULL, 'Velit possimus eum ea aut.', 'Laboriosam aspernatur laudantium similique maxime. Harum libero esse nisi libero. Soluta et earum quaerat officiis nam.', 2),
(7, NULL, NULL, 'Impedit ex et exercitationem.', 'Rem quis dignissimos qui. Maxime inventore ex sint omnis non iure et. Cum qui et ut est. Sint facilis omnis quia quo nesciunt quia.', 2),
(8, NULL, NULL, 'Quis doloremque atque quasi.', 'Aperiam officiis est corrupti earum molestiae cupiditate maxime. Sunt dolorum ex iusto voluptatem. Aut iste molestias illum. Quibusdam vel accusantium veniam hic sed qui voluptas.', 4),
(9, NULL, NULL, 'Non totam a assumenda.', 'Eos recusandae quia repellendus recusandae quia placeat aperiam. Debitis possimus et atque dolorem ducimus. Exercitationem occaecati aut aut provident et occaecati. Consequatur sunt asperiores nisi ut.', 3),
(10, NULL, NULL, 'Voluptatem corrupti voluptatum.', 'Molestias perferendis aliquam provident ipsam ea et enim deserunt. Est pariatur repellat ut odit laboriosam ipsam.', 2),
(11, NULL, NULL, 'Non repudiandae sunt et recusandae consequuntur.', 'Non similique et exercitationem sint modi officiis. Dolores doloremque voluptas possimus quos non praesentium. Tempore doloremque a ut perferendis laborum dolores dolor. Libero qui sunt neque occaecati. Est totam velit ut quia.', 2),
(12, NULL, NULL, 'Tempora et asperiores cum.', 'Sed sit nesciunt tenetur et. Repudiandae iusto molestias velit. Et est perferendis id ea quia facilis. Nemo et fuga perferendis quia veritatis iste in.', 3),
(13, NULL, NULL, 'Vero voluptas sapiente odio velit.', 'Atque a incidunt placeat rerum et sapiente. Rerum sint iusto expedita fugiat est consequatur. Vitae consectetur excepturi dolorem similique totam et.', 2),
(14, NULL, NULL, 'Fugiat consequatur veniam hic.', 'Suscipit alias est inventore consectetur nobis. Ea placeat occaecati vel placeat ut. Doloribus et amet voluptas nesciunt. Ab expedita ut autem inventore illum et.', 2),
(15, NULL, NULL, 'Dolor expedita nesciunt tempora eos.', 'Id veniam ut architecto temporibus repellendus iure. Et beatae voluptate similique.', 2),
(16, NULL, NULL, 'Eaque tenetur soluta praesentium sequi.', 'Perspiciatis alias facere excepturi repellat dolorum rerum. Repellendus facilis aliquam non minus mollitia sit assumenda. Non dolorem nihil molestias ut explicabo esse quibusdam. Aut numquam culpa qui modi.', 2),
(17, NULL, NULL, 'Autem qui qui.', 'Qui dolorum eaque et laborum debitis exercitationem placeat. Est et ratione autem et ipsa.', 3),
(18, NULL, NULL, 'Quo qui est dolorum maiores.', 'Sit ipsam ipsum earum nobis dicta. Facere rerum dolores qui dolores voluptate dolor voluptas. Totam quasi et perferendis expedita sit. Natus omnis laborum nobis eaque et ea tempora.', 3),
(19, NULL, NULL, 'Et soluta blanditiis.', 'Blanditiis nihil debitis enim consequatur. Omnis dolorum enim doloribus impedit id. Fugiat eum ipsam esse. Eum molestiae rerum est sed.', 3),
(20, NULL, NULL, 'Doloribus distinctio rerum commodi adipisci.', 'Ipsum assumenda nesciunt repudiandae amet sit sit eius. Voluptatibus aut voluptate molestias ut recusandae. Et natus voluptates et. Laborum error aut aut vitae.', 4),
(21, NULL, NULL, 'Voluptatem maiores minima.', 'Provident velit sapiente iusto fugiat. Inventore laborum esse ipsa nihil laboriosam laboriosam est. Sit qui iste natus occaecati veniam labore. Odit error ullam eum tempora. Qui officiis alias sequi numquam voluptatem neque aliquam.', 4),
(22, NULL, NULL, 'Qui exercitationem sapiente dolorem.', 'Facere ab repellendus sed recusandae in et. Magni est voluptatibus provident possimus similique deserunt ipsum in.', 2),
(23, NULL, NULL, 'Voluptatem iure laboriosam.', 'Dolore incidunt numquam molestiae accusantium natus dolor quae. Ut eius soluta voluptatem voluptas dolores expedita reprehenderit. Hic qui pariatur voluptas voluptate explicabo. Necessitatibus repellat molestias ipsa qui culpa accusantium laudantium.', 4),
(24, NULL, NULL, 'Consequatur et sed.', 'Accusamus et magni id cumque ut. Rerum laboriosam nam alias. Voluptatem esse doloremque fugit.', 1),
(25, NULL, NULL, 'Sit voluptatem reprehenderit.', 'Voluptatem et alias aut harum harum qui earum. Earum tenetur architecto aliquam id rerum impedit. Explicabo autem sit qui enim voluptate.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `folder`
--

CREATE TABLE `folder` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED DEFAULT NULL,
  `resource` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folder`
--

INSERT INTO `folder` (`id`, `created_at`, `updated_at`, `name`, `folder_id`, `resource`) VALUES
(1, NULL, NULL, 'root', NULL, NULL),
(2, NULL, NULL, 'resource', 1, 1),
(3, NULL, NULL, 'documents', 1, NULL),
(4, NULL, NULL, 'graphics', 1, NULL),
(5, NULL, NULL, 'other', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  `pagination` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `created_at`, `updated_at`, `name`, `table_name`, `read`, `edit`, `add`, `delete`, `pagination`) VALUES
(1, NULL, NULL, 'Example', 'example', 1, 1, 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `form_field`
--

CREATE TABLE `form_field` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browse` tinyint(1) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `relation_table` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relation_column` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` int(10) UNSIGNED NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `form_field`
--

INSERT INTO `form_field` (`id`, `created_at`, `updated_at`, `name`, `type`, `browse`, `read`, `edit`, `add`, `relation_table`, `relation_column`, `form_id`, `column_name`) VALUES
(1, NULL, NULL, 'Title', 'text', 1, 1, 1, 1, NULL, NULL, 1, 'name'),
(2, NULL, NULL, 'Description', 'text_area', 1, 1, 1, 1, NULL, NULL, 1, 'description'),
(3, NULL, NULL, 'Status', 'relation_select', 1, 1, 1, 1, 'status', 'name', 1, 'status_id');

-- --------------------------------------------------------

--
-- Table structure for table `frontend_users`
--

CREATE TABLE `frontend_users` (
  `id` int(11) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `user_type` enum('customer','redgenie','salesgenie') NOT NULL DEFAULT 'customer',
  `password` text DEFAULT NULL,
  `api_token` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`manipulations`)),
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`custom_properties`)),
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`responsive_images`)),
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menulist`
--

CREATE TABLE `menulist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menulist`
--

INSERT INTO `menulist` (`id`, `name`) VALUES
(1, 'sidebar menu'),
(2, 'top menu');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `href`, `icon`, `slug`, `parent_id`, `menu_id`, `sequence`) VALUES
(1, 'Dashboard', '/admin/', 'cil-speedometer', 'link', NULL, 1, 1),
(4, 'Admin Users', '/admin/users', 'cil-user', 'link', NULL, 1, 3),
(6, 'Edit menu elements', '/menu/element', NULL, 'link', 2, 1, 2),
(16, 'Breadcrumb', '/base/breadcrumb', NULL, 'link', 15, 1, 17),
(17, 'Cards', '/base/cards', NULL, 'link', 15, 1, 18),
(18, 'Carousel', '/base/carousel', NULL, 'link', 15, 1, 19),
(19, 'Collapse', '/base/collapse', NULL, 'link', 15, 1, 20),
(20, 'Forms', '/base/forms', NULL, 'link', 15, 1, 21),
(21, 'Jumbotron', '/base/jumbotron', NULL, 'link', 15, 1, 22),
(22, 'List group', '/base/list-group', NULL, 'link', 15, 1, 23),
(23, 'Navs', '/base/navs', NULL, 'link', 15, 1, 24),
(24, 'Pagination', '/base/pagination', NULL, 'link', 15, 1, 25),
(25, 'Popovers', '/base/popovers', NULL, 'link', 15, 1, 26),
(26, 'Progress', '/base/progress', NULL, 'link', 15, 1, 27),
(27, 'Scrollspy', '/base/scrollspy', NULL, 'link', 15, 1, 28),
(28, 'Switches', '/base/switches', NULL, 'link', 15, 1, 29),
(29, 'Tables', '/base/tables', NULL, 'link', 15, 1, 30),
(30, 'Tabs', '/base/tabs', NULL, 'link', 15, 1, 31),
(31, 'Tooltips', '/base/tooltips', NULL, 'link', 15, 1, 32),
(56, 'Dashboard', '/', NULL, 'link', 55, 2, 56),
(57, 'Notes', '/notes', NULL, 'link', 55, 2, 57),
(58, 'Users', '/users', NULL, 'link', 55, 2, 58),
(60, 'Edit menu', '/menu/menu', NULL, 'link', 59, 2, 60),
(61, 'Edit menu elements', '/menu/element', NULL, 'link', 59, 2, 61),
(62, 'Edit roles', '/roles', NULL, 'link', 59, 2, 62),
(63, 'Media', '/media', NULL, 'link', 59, 2, 63),
(64, 'BREAD', '/bread', NULL, 'link', 59, 2, 64),
(65, 'Red Genie', '/admin/red_genie', 'cil-user', 'link', NULL, 1, 6),
(66, 'Sales Genie', '/admin/sales_genie', 'cil-user', 'link', NULL, 1, 33),
(67, 'Settings', '/admin/settings', 'cil-settings', 'link', NULL, 1, 34);

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE `menu_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menus_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`id`, `role_name`, `menus_id`) VALUES
(22, 'user', 16),
(23, 'admin', 16),
(24, 'user', 17),
(25, 'admin', 17),
(26, 'user', 18),
(27, 'admin', 18),
(28, 'user', 19),
(29, 'admin', 19),
(30, 'user', 20),
(31, 'admin', 20),
(32, 'user', 21),
(33, 'admin', 21),
(34, 'user', 22),
(35, 'admin', 22),
(36, 'user', 23),
(37, 'admin', 23),
(38, 'user', 24),
(39, 'admin', 24),
(40, 'user', 25),
(41, 'admin', 25),
(42, 'user', 26),
(43, 'admin', 26),
(44, 'user', 27),
(45, 'admin', 27),
(46, 'user', 28),
(47, 'admin', 28),
(48, 'user', 29),
(49, 'admin', 29),
(50, 'user', 30),
(51, 'admin', 30),
(52, 'user', 31),
(53, 'admin', 31),
(105, 'guest', 56),
(106, 'user', 56),
(107, 'admin', 56),
(108, 'user', 57),
(109, 'admin', 57),
(110, 'admin', 58),
(112, 'admin', 60),
(113, 'admin', 61),
(114, 'admin', 62),
(115, 'admin', 63),
(116, 'admin', 64),
(122, 'admin', 6),
(146, 'admin', 1),
(147, 'user', 1),
(148, 'admin', 4),
(149, 'admin', 65),
(150, 'user', 65),
(151, 'admin', 66),
(152, 'user', 66),
(153, 'admin', 67),
(154, 'user', 67);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_11_085455_create_notes_table', 1),
(5, '2019_10_12_115248_create_status_table', 1),
(6, '2019_11_08_102827_create_menus_table', 1),
(7, '2019_11_13_092213_create_menurole_table', 1),
(8, '2019_12_10_092113_create_permission_tables', 1),
(9, '2019_12_11_091036_create_menulist_table', 1),
(10, '2019_12_18_092518_create_role_hierarchy_table', 1),
(11, '2020_01_07_093259_create_folder_table', 1),
(12, '2020_01_08_184500_create_media_table', 1),
(13, '2020_01_21_161241_create_form_field_table', 1),
(14, '2020_01_21_161242_create_form_table', 1),
(15, '2020_01_21_161243_create_example_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 15),
(1, 'App\\User', 17),
(1, 'App\\User', 19),
(1, 'App\\User', 20),
(2, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 4),
(2, 'App\\User', 5),
(2, 'App\\User', 6),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 11),
(2, 'App\\User', 12),
(2, 'App\\User', 13),
(2, 'App\\User', 16),
(2, 'App\\User', 19),
(4, 'App\\User', 14);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sherin.taurus@gmail.com', '$2y$10$rtcDZQ9dW3q2eg9rq.evbOeybFNNp7IX9hYS/01STlPIkfw.TBXRu', '2020-03-19 00:49:33');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'browse bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(2, 'read bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(3, 'edit bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(4, 'add bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25'),
(5, 'delete bread 1', 'web', '2020-02-20 00:47:25', '2020-02-20 00:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `red_genies`
--

CREATE TABLE `red_genies` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `fk_frontend_user_id` int(11) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-02-20 00:47:14', '2020-02-20 00:47:14'),
(2, 'user', 'web', '2020-02-20 00:47:14', '2020-02-20 00:47:14'),
(3, 'guest', 'web', '2020-02-20 00:47:14', '2020-02-20 00:47:14'),
(4, 'redgenie', 'web', '2020-03-12 00:00:34', '2020-03-12 00:00:34'),
(5, 'salesgenie', 'web', '2020-03-12 00:00:46', '2020-03-12 00:00:46'),
(6, 'customer', 'web', '2020-03-12 00:00:58', '2020-03-12 00:00:58');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `role_hierarchy`
--

CREATE TABLE `role_hierarchy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `hierarchy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_hierarchy`
--

INSERT INTO `role_hierarchy` (`id`, `role_id`, `hierarchy`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sales_genies`
--

CREATE TABLE `sales_genies` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `fk_frontend_user_id` int(11) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 1 COMMENT '1-salesgenie, 2-salesgenie plus\r\n',
  `fk_red_genie_id` int(11) NOT NULL,
  `mobile` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `settings` varchar(150) NOT NULL,
  `value` decimal(5,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `settings`, `value`, `created_at`, `updated_at`) VALUES
(1, 'pay out %\r\n', '55.00', NULL, '2020-03-09 23:23:33');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `class`) VALUES
(1, 'ongoing', 'badge badge-pill badge-primary'),
(2, 'stopped', 'badge badge-pill badge-secondary'),
(3, 'completed', 'badge badge-pill badge-success'),
(4, 'expired', 'badge badge-pill badge-warning');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuroles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `menuroles`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@refer2gain.com', '2020-02-20 00:47:14', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user,admin', 'XCIEMlIoxTVzTJDGOmnp6B41FAMySAGKuB2CKpTmVqEMlt1lndwdxSiGNIla', '2020-02-20 00:47:14', '2020-03-19 03:17:33', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `example`
--
ALTER TABLE `example`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folder`
--
ALTER TABLE `folder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_field`
--
ALTER TABLE `form_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_users`
--
ALTER TABLE `frontend_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `menulist`
--
ALTER TABLE `menulist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `red_genies`
--
ALTER TABLE `red_genies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_genies`
--
ALTER TABLE `sales_genies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `example`
--
ALTER TABLE `example`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `folder`
--
ALTER TABLE `folder`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `form_field`
--
ALTER TABLE `form_field`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `frontend_users`
--
ALTER TABLE `frontend_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menulist`
--
ALTER TABLE `menulist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `menu_role`
--
ALTER TABLE `menu_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `red_genies`
--
ALTER TABLE `red_genies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sales_genies`
--
ALTER TABLE `sales_genies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
