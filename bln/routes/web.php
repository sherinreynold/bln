<?php
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
 }

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});




Route::prefix('admin/')->group(function () {
Route::group(['middleware' => ['get.menu']], function () {
    Route::get('/home', function () {           return view('dashboard.homepage'); })->middleware('auth');

    Route::group(['middleware' => ['role:user']], function () {
        
        Route::get('/404', function () {        return view('dashboard.404'); });
        Route::get('/500', function () {        return view('dashboard.500'); });

		Route::resource('red_genie', 'RedGenieController');
		Route::get('/red_genie/reg_links/{redgenieId}', 'RedGenieController@regLinks')->name('red_genie.reg_links');
		Route::get('/red_genie/add_reg_links/{redgenieId}', 'RedGenieController@addRegLinks');
		Route::post('/red_genie/save_reg_links/{redgenieId}', 'RedGenieController@saveRegLinks');
		
		
		Route::resource('sales_genie', 'SalesGenieController');
		Route::get('/sales_genie/reg_links/{redgenieId}', 'SalesGenieController@regLinks')->name('sales_genie.reg_links');
		Route::get('/sales_genie/add_reg_links/{redgenieId}', 'SalesGenieController@addRegLinks');
        Route::post('/sales_genie/save_reg_links/{redgenieId}', 'SalesGenieController@saveRegLinks');
        Route::post('/sales_genie/delete_reg_links/{id}/{sg_id}', 'SalesGenieController@deleteRegLinks')->name('reg_links.destroy');
        
        Route::resource('customers', 'CustomerController');
        
        Route::resource('settings', 'SettingsController');
        
        Route::resource('resource_center', 'ResourceCenterController');

        Route::get('credits', 'CreditController@index')->name('credits.index');
        Route::get('credits/{refferal_code}', 'CreditController@view');
        Route::get('points/create', 'PointController@create')->name('points.create');
        Route::post('points/store', 'PointController@store')->name('points.store');
        Route::get('points', 'PointController@index')->name('points.index');
        Route::get('points/{invoice_no}', 'PointController@view');
        Route::get('points/{id}/edit', 'PointController@edit');
        Route::post('points/{id}/update', 'PointController@update');
		
		Route::get('network', 'NetworkController@index');
        Route::get('network_tree_view', 'NetworkController@network_tree_view')->name('network_tree_view');
		
		
    });
    Auth::routes();

    Route::resource('resource/{table}/resource', 'ResourceController')->names([
        'index'     => 'resource.index',
        'create'    => 'resource.create',
        'store'     => 'resource.store',
        'show'      => 'resource.show',
        'edit'      => 'resource.edit',
        'update'    => 'resource.update',
        'destroy'   => 'resource.destroy'
    ]);

    Route::group(['middleware' => ['role:admin']], function () {
        
        Route::resource('users',        'UsersController');
        Route::resource('roles',        'RolesController');
        Route::get('/roles/move/move-up',      'RolesController@moveUp')->name('roles.up');
        Route::get('/roles/move/move-down',    'RolesController@moveDown')->name('roles.down');
        Route::prefix('menu/element')->group(function () { 
            Route::get('/',             'MenuElementController@index')->name('menu.index');
            Route::get('/move-up',      'MenuElementController@moveUp')->name('menu.up');
            Route::get('/move-down',    'MenuElementController@moveDown')->name('menu.down');
            Route::get('/create',       'MenuElementController@create')->name('menu.create');
            Route::post('/store',       'MenuElementController@store')->name('menu.store');
            Route::get('/get-parents',  'MenuElementController@getParents');
            Route::get('/edit',         'MenuElementController@edit')->name('menu.edit');
            Route::post('/update',      'MenuElementController@update')->name('menu.update');
            Route::get('/show',         'MenuElementController@show')->name('menu.show');
            Route::get('/delete',       'MenuElementController@delete')->name('menu.delete');
        });
        Route::prefix('menu/menu')->group(function () { 
            Route::get('/',         'MenuController@index')->name('menu.menu.index');
            Route::get('/create',   'MenuController@create')->name('menu.menu.create');
            Route::post('/store',   'MenuController@store')->name('menu.menu.store');
            Route::get('/edit',     'MenuController@edit')->name('menu.menu.edit');
            Route::post('/update',  'MenuController@update')->name('menu.menu.update');
            Route::get('/delete',   'MenuController@delete')->name('menu.menu.delete');
        });
        
    });
});


});


//frontend

Route::get('/signup', function(){return view('frontend.login.signup');})->name('frontend.signup');
Route::post('/signup', 'frontend\CustomersController@register');


Route::get('/', 'Auth\FrontendLoginController@showLoginForm');
Route::get('/login', 'Auth\FrontendLoginController@showLoginForm')->name('frontend.login');
Route::post('/login', 'Auth\FrontendLoginController@login')->name('frontend.login.post');
Route::post('/logout', 'Auth\FrontendLoginController@logout')->name('frontend.logout');

Route::get('/registration/{refId}', 'frontend\CustomersController@completeRegistration')->name('complete_registration');
Route::post('/registration/{refId}', 'frontend\CustomersController@updateRegistration');


//password reset routes
Route::post('reset_password_without_token', 'frontend\AccountsController@validatePasswordRequest');
Route::post('reset_password_with_token', 'frontend\AccountsController@resetPassword');
Route::get('/forgot-password', 'frontend\AccountsController@forgot_password');
Route::get('password/reset/{token}', 'frontend\AccountsController@showPasswordResetForm');
Route::post('reset-password/{token}', 'frontend\AccountsController@resetPassword_submit')->name('my_route');




Route::group(['middleware'=>'frontend'], function() {
    Route::get('/profile', 'frontend\ProfileController@index')->name('profile');
	Route::post('/profile_update', 'frontend\ProfileController@update');
    Route::post('/exp_update', 'frontend\ProfileController@expUpdate');
    Route::get('/exp_remove/{id}', 'frontend\ProfileController@expRemove');
    
    Route::get('/resource_center', 'frontend\ProfileController@resourceCenter')->name('resource_center');
    Route::get('/statements', 'frontend\StatementController@index')->name('statements');
    Route::post('/statements', 'frontend\StatementController@index');

    Route::get('/statements/credits', 'frontend\StatementController@credits')->name('statements.credits');
    Route::post('/statements/credits', 'frontend\StatementController@credits');

    Route::get('/create_salesgenie', 'frontend\FrontendSalesGenieController@create')->name('create_salesgenie');
    Route::post('/create_salesgenie', 'frontend\FrontendSalesGenieController@store')->name('add_salesgenie');

    Route::get('/invites', 'frontend\FrontendSalesGenieController@invites')->name('invites');
    Route::post('/invites', 'frontend\FrontendSalesGenieController@addInvites')->name('add_invites');
	
	//network routes
    Route::get('/network', 'frontend\ProfileController@network')->name('network');
    Route::post('/ajax_network', 'frontend\ProfileController@customers_row');

    
    Route::post('/crop-image', 'frontend\ImageController@uploadImage')->name('upload.image');
});

Route::get('/get_profile_pic/{id}/{opt?}', 'frontend\ProfileController@getProfilePic');

//Apis

Route::post('/profile-verification', 'frontend\RedCreditsController@add')->name('add_red_credit');
Route::post('/payment_completion', 'frontend\RedPointsController@add')->name('add_red_point');



