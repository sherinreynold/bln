@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> Sales Genie : {{ $note[0]->name }}</div>
                    <div class="card-body">
					
					<table style="width:100%;margin-bottom:25px;">
		<tr><td><h4>Code</h4></td><td><h4>: {{ $note[0]->code }}</h4></td></tr>			
		<tr><td><h4> Name</h4></td><td><h4>: {{ $note[0]->name }}</h4></td></tr>
		<!--<tr><td><h4>Last Name</h4></td><td><h4>: {{ $note[0]->last_name }}</h4></td></tr>-->
		<tr><td><h4>Email</h4></td><td><h4>: {{ $note[0]->email }}</h4></td></tr>
		<tr><td><h4>Mobile</h4></td><td><h4>: {{ $note[0]->phone }}</h4></td></tr>
		<tr><td><h4>Address</h4></td><td><h4>: {{ $note[0]->address }}</h4></td></tr>
		<!--<tr><td><h4>City</h4></td><td><h4>: {{ $note[0]->city }}</h4></td></tr>-->
		@if($note[0]->bln_role_id == '2')
		<tr><td><h4>Parent [RG]</h4></td><td><h4>: {{ $note[0]->parent_name }} - {{ $note[0]->parent_code }}</h4></td></tr>
    @endif
    @if($note[0]->bln_role_id == '3')
		<tr><td><h4>Parent [SG+]</h4></td><td><h4>: {{ $note[0]->parent_name }} - {{ $note[0]->parent_code }}</h4></td></tr>
    @endif
		<tr><td><h4>Type</h4></td><td><h4>: @if($note[0]->bln_role_id ==3) Sales Genie @elseif($note[0]->bln_role_id ==2) Sales Genie + @endif</h4></td></tr>
	<!--	<tr><td><h4>State</h4></td><td><h4>: {{ $note[0]->state }}</h4></td></tr>
		<tr><td><h4>Zip</h4></td><td><h4>: {{ $note[0]->zip }}</h4></td></tr>-->
					
					</table>
                       
                        
                        <a href="{{ route('sales_genie.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection