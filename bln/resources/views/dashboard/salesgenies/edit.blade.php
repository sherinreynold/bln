@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Edit') }}: {{ $note->first_name }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('admin/sales_genie/'. $note->id) }}">
                            @csrf
                            @method('PUT')
                            

                            <div class="form-group row">
                                <label>Name</label>
                                <input class="form-control" type="text" placeholder="{{ __('First Name') }}" name="f_name" required autofocus value="{{ $note->name }}">
                            </div>
							<!--<div class="form-group row">
                                <label>Last Name</label>
                                <input class="form-control" type="text" placeholder="{{ __('Last Name') }}" name="l_name" required autofocus value="{{ $note->last_name }}">
                            </div>-->
							
							<div class="form-group row">
                                <label>Email Id</label>
                                <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus value="{{ $note->email }}">
                            </div>
							
							<div class="form-group row">
                                <label>Mobile</label>
                                <input class="form-control" type="mobile" placeholder="{{ __('Mobile') }}" name="mobile" required autofocus value="{{ $note->phone }}" onkeypress="return isNmb(event)">
                            </div>


                            <!--<div class="form-group row">
                                <label>Parent </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="parent" value='r' @if($note->fk_red_genie_id !=0) checked @endif>Redgenie&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="parent" value='s' @if($note->fk_sales_genie_id !=0) checked @endif>Salesgenie +

                                

                                </div>-->

                                <div class="form-group row">
                                <label>Type</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="type" id="sg_type" required autofocus>
							
							<option @if( $note->bln_role_id ==3) selected @endif value='1'>Sales Genie</option>
							<option @if( $note->bln_role_id ==2) selected @endif value='2'>Sales Genie +</option>
							</select>
							</div>
							
							
							<div class="form-group row rg">
                                <label>Red Genie</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="redgenie_id" id="rg_drp" required autofocus>
							<option value=''>--Select Red Genie--</option>
							<?php if($redgenies)
							{
								foreach($redgenies as $redgenie)
								{
								?>
							<option @if($redgenie['id'] ==$note->parent_id) selected @endif value="{{ $redgenie['id'] }}">{{ $redgenie['name'] }} - {{ $redgenie['code'] }}</option>
							<?php } } ?>
							
							</select>
							</div>


                            <div class="form-group row sg_plus" style='display:none;'>
                                <label>Sales Genie+</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="salesgenieplus_id" id="sg_drp"  autofocus>
							<option value=''>--Select Sales Genie +--</option>
							<?php if($salesgenie_plus)
							{
								foreach($salesgenie_plus as $sgp)
								{
								?>
							<option @if($sgp['id'] ==$note->parent_id) selected @endif value="{{ $sgp['id'] }}">{{ $sgp['name'] }} - {{ $sgp['code'] }}</option>
							<?php } } ?>
							
							</select>
							</div>
							
							
							
							
							
							<div class="form-group row">
                                <label>Address</label>
                                <input class="form-control" type="text" placeholder="{{ __('Address') }}" name="address" required autofocus value="{{ $note->address }}">
                            </div>
							<!--<div class="form-group row">
                                <label>City</label>
                                <input class="form-control" type="text" placeholder="{{ __('City') }}" name="city" required autofocus value="{{ $note->city }}">
                            </div>
							
							<div class="form-group row">
                                <label>State</label>
                                <input class="form-control" type="text" placeholder="{{ __('State') }}" name="state" required autofocus value="{{ $note->state }}">
                            </div>
							
							<div class="form-group row">
                                <label>Zip</label>
                                <input class="form-control" type="text" placeholder="{{ __('Zip') }}" name="zip" required autofocus value="{{ $note->zip }}" onkeypress="return isNmb(event)">
                            </div>-->
							
							
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                            <a href="{{ route('sales_genie.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')
<script>
$('#sg_type').change(function() {

var val=$(this).val();
if(val=='1'){
    $(".sg_plus").show();
    $('#sg_drp').prop('required',true);
    $('#rg_drp').removeAttr('required');
    $(".rg").hide();
   
}else{
    $(".sg_plus").hide();
    $('#sg_drp').removeAttr('required');
    $('#rg_drp').prop('required',true);
    $(".rg").show();
    
}

});

$("document").ready(function(){

var val=$('#sg_type').val();
if(val=='1'){
    $(".sg_plus").show();
    $('#sg_drp').prop('required',true);
    $('#rg_drp').removeAttr('required');
    $(".rg").hide();
   
}else{
    $(".sg_plus").hide();
    $('#sg_drp').removeAttr('required');
    $('#rg_drp').prop('required',true);
    $(".rg").show();
    
}
});


</script>

@endsection