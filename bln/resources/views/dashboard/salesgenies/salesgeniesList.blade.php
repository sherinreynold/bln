@extends('dashboard.base')

@section('content')



        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Sales Genie') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('sales_genie.create') }}" class="btn btn-primary m-2">{{ __('Add Sales Genie') }}</a>
                        </div>
						
						
						 <div class="row mb-3">
                    <div class="col-sm-8">
                        <form action="{{ url('admin/sales_genie') }}">
						<table style="width: 50%;margin-left: -8px;">
						<tr>
						<td>
							<input type="text" name="code" class="form-control" placeholder="Code" value="{{@$_GET['code']}}">
							
                           </td>
                            <td>
							
							<select class="form-control" name="type">
                              
                              <option value="" @if($type_filter=='') selected @endif>All Types</option>							  
                              <option value="3" @if($type_filter=='3') selected @endif>Sales Genie</option>
							  <option value="2" @if($type_filter=='2') selected @endif>Sales Genie+</option>
                            </select>
                           </td>
						   <td>
                            <button type="submit" class="btn btn-secondary">Filter</button>
							</td>
							</tr>
							</table>
							
                        </form>
                    </div>
                </div>
						
						
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
							<th>Type</th>
                            
							<th></th>
							<th></th>
							<th></th>
							<th></th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($notes as $note)
                            <tr>
                              <td><strong>{{ $note->code }}</strong></td>
                              <td><strong>{{ $note->name }}</strong></td>
                              <td>{{ $note->email }}</td>
							  <td>@if($note->bln_role_id=='3')
								  Sales Genie
							  @else
							  <b>Sales Genie+</b>
							  @endif
							  </td>
                              
                              
							  <td>
                                <a href="{{ url('admin/sales_genie/reg_links/' . $note->id) }}" class="btn btn-block btn-primary">Reg Links</a>
                              </td>
                              <td>
                                <a href="{{ url('admin/sales_genie/' . $note->id) }}" class="btn btn-block btn-primary">View</a>
                              </td>
                              <td>
                                
                                <a href="{{ url('admin/sales_genie/' . $note->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                              
                              </td>
                              <td>
                                <form action="{{ route('sales_genie.destroy', $note->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $notes->appends($_GET)->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

