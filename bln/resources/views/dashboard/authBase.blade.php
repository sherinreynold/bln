<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.0.0-alpha.1
* @link https://coreui.io
* Copyright (c) 2019 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
    

  <!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css"> 

<title>Refer to Gain Admin</title>
  </head>
<body>

<!--============head_top============-->
<section class="head_top">
  
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3"> <img src="img/logo.png" class="img-fluid logo"> 
      </div>
       <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
      </div>
    </div>
  </div>

</section>
  

    @yield('content') 

    <!--============footer============-->
<section class="footer">
  
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6"><p class="ftr_left">©2020 Redlogik - All Rights Reserved</p> 
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6"><p class="ftr_right">Terms of Use | Privacy Policy</p> 
    </div>
  </div>
</div>

</section>

<!--============footer============-->

    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('js/pace.min.js') }}"></script> 
    <script src="{{ asset('js/coreui.bundle.min.js') }}"></script>

    @yield('javascript')

  </body>
</html>
