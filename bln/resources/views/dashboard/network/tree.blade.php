@extends('dashboard.base')
@section('content')
<div class="container-fluid">
  <div class="fade-in">
    <style type="text/css">
      .text-red-genie{
        color: red !important;
      }
      .text-sales-genie{
        color: green !important;
      }
      .text-sales-genie-plus{
        color: yellow !important;
      }
      /*.text-customers{
      color: red !important;
      }*/
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">-->

   
    <div id="tree-container">
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script src="{{ asset('js/Chart.min.js') }}">
</script>
<script src="{{ asset('js/coreui-chartjs.bundle.js') }}">
</script>
<script src="{{ asset('js/main.js') }}" defer>
</script>


    <script type="text/javascript">
      $(document).ready(function(){
        //fill data to tree  with AJAX call
        var url="{{ url('admin/network_tree_view') }}";
        // alert(ur)
        $('#tree-container').jstree({
          'types': {
            "default": {
              icon: "fa fa-folder text-warning fa-lg"
            }
            ,
            file: {
              icon: "fa fa-file text-info fa-lg"
            }
            ,
            red_genies: {
              icon: "fa fa-folder text-info text-red-genie fa-lg"
            }
            ,
            sales_genies: {
              icon: "fa fa-folder text-info text-sales-genie fa-lg"
            }
            ,
            sales_genies_plus: {
              icon: "fa fa-folder text-warning fa-lg"
            }
          }
          ,
          'plugins': ["wholerow", "types"],
          'core' : {
            'data' : {
              "url" : url,
              'plugins': ["wholerow", "types"],
              "dataType" : "json" // needed only if you do not supply JSON headers
            }
          }
          ,
        }).bind("select_node.jstree", function (e, data) {
          var href = data.node.a_attr.href;
          if(href == '#')
            return '';
          document.location.href = href;
        });
      });
    </script>
	
	
	
@endsection