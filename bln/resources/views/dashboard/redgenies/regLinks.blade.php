@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Registration Links') }}</div>
                    <div class="card-body">
                        <div class="row"> 
			<a href="{{url('/red_genie')}}" class="btn btn-primary m-2">{{ __('< Back') }}</a>
						
                          <a href="{{url('/red_genie/add_reg_links/'.$redgenie_id)}}" class="btn btn-primary m-2">{{ __('Generate Link') }}</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
							<th>Email</th>
							
                            
							
							<th>Link</th>
							
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($customer_links as $note)
                            <tr>
                              <td><strong>{{ $note->name }}</strong></td>
                              <td>{{ $note->email }}</td>
                              
                              <td>{{url('registration/'.base64_encode($note->id)) }}</td>
							  
							 
                              <!--<td>
                                <form action="{{ route('red_genie.destroy', $note->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>
                              </td>-->
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $customer_links->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

