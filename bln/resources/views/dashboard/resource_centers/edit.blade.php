@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Edit Resource') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{url('admin/resource_center/'.$rc->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label> Title</label>
                                <input class="form-control" type="text" placeholder="Title" name="title" value="{{@ $rc->title}}" required autofocus>
                            </div>
							
							
							<div class="form-group row">
                                <label>Link</label>
                                <input class="form-control" type="text" placeholder="link" name="link" value="{{@ $rc->link}}" required autofocus>
                            </div>
							
							<div class="form-group row">
                                <label>Type</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="type" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='1' @if($rc->type==1) selected @endif >Training Manual</option>
							<option value='2' @if($rc->type==2) selected @endif >Articles</option>
                            <option value='3' @if($rc->type==3) selected @endif >Case Studies</option>
							<option value='4' @if($rc->type==4) selected @endif >Videos</option>
							</select>
							</div>

                            <div class="form-group row">
                                <label>For</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="r_for" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='1' @if($rc->resource_for==1) selected @endif >Red Genies</option>
							<option value='2' @if($rc->resource_for==2) selected @endif >Sales Genies </option>
                            <option value='3' @if($rc->resource_for==3) selected @endif >Customers</option>
							</select>
							</div>

                            <div class="form-group row">
                                <label>Featured</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="featured" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='0' @if($rc->is_featured==0) selected @endif >False</option>
							<option value='1' @if($rc->is_featured==1) selected @endif >True</option>
							</select>
							</div>

                            <div class="form-group row">
                                <label>Status</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="status" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='0' @if($rc->status==0) selected @endif >Disabled</option>
							<option value='1' @if($rc->status==1) selected @endif >Enabled</option>
							</select>
							</div>

                           
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                            <a href="{{ route('resource_center.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection