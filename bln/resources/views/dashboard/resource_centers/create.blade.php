@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Create Resource') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('resource_center.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label> Title</label>
                                <input class="form-control" type="text" placeholder="Title" name="title" required autofocus>
                            </div>
							
							
							<div class="form-group row">
                                <label>Link</label>
                                <input class="form-control" type="text" placeholder="link" name="link" required autofocus>
                            </div>
							
							<div class="form-group row">
                                <label>Type</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="type" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='1'>Training Manual</option>
							<option value='2'>Articles</option>
                            <option value='3'>Case Studies</option>
							<option value='4'>Videos</option>
							</select>
							</div>

                            <div class="form-group row">
                                <label>For</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="r_for" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='1'>Red Genies</option>
							<option value='2'>Sales Genies </option>
                            <option value='3'>Customers</option>
							</select>
							</div>

                            <div class="form-group row">
                                <label>Featured</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="featured" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='0'>False</option>
							<option value='1'>True</option>
							</select>
							</div>

                            <div class="form-group row">
                                <label>Status</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="status" required autofocus>
							<option value=''>--Select Type--</option>
							<option value='0'>Disabled</option>
							<option value='1'>Enabled</option>
							</select>
							</div>

                           
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('resource_center.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection