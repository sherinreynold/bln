@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> Resource</div>
                    <div class="card-body">
					
<?php

switch ($rc->type) {
  case "1":
      $rc_type = "Training Manual";
  break;
  case "2":
      $rc_type = "Articles";
  break;
  case "3":
      $rc_type = "Case Studies";
  break;
  case "4":
      $rc_type = "Videos";
  break;
  default:
  $rc_type = "";
  break;
};

switch ($rc->resource_for) {
  case "1":
      $rc_resource_for = "Red Genies";
  break;
  case "2":
      $rc_resource_for = "Sales Genies";
  break;
  case "3":
      $rc_resource_for = "Customers";
  
  
};

switch ($rc->is_featured) {
  case "0":
      $rc_is_featured = "No";
  break;
  case "1":
      $rc_is_featured = "Yes";
  
  
};

switch ($rc->status) {
  case "0":
      $rc_status = "Disabled";
  break;
  case "1":
      $rc_status = "Enabled";
  
  
};

echo 'sd'.$rc_resource_for;
?>

					<table style="width:100%;margin-bottom:25px;">
		<tr><td><h4>Title</h4></td><td><h4>: {{ $rc->title }}</h4></td></tr>	
		<tr><td><h4>Link</h4></td><td><h4>: {{ $rc->link }}</h4></td></tr>
		<tr><td><h4>Type</h4></td><td><h4>: {{ $rc_type }}</h4></td></tr>
		<tr><td><h4>For</h4></td><td><h4>: {{ $rc_resource_for }}</h4></td></tr>
		<tr><td><h4>Featured</h4></td><td><h4>: {{ $rc_is_featured }}</h4></td></tr>
		<tr><td><h4>Status</h4></td><td><h4>: {{ $rc_status }}</h4></td></tr>
		
					
					</table>
                       
                        
                        <a href="{{ route('resource_center.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection