@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Resource Centre') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('resource_center.create') }}" class="btn btn-primary m-2">{{ __('Add New') }}</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Link</th>
                            <th>Type</th>
                            <th>For</th>
                            <th>Status</th>
                            
                            
							<th></th>
							<th></th>
							<th></th>
							<th></th>
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($rcs as $rc)


                          <?php

switch ($rc->type) {
  case "1":
      $rc_type = "Training Manual";
  break;
  case "2":
      $rc_type = "Articles";
  break;
  case "3":
      $rc_type = "Case Studies";
  break;
  case "4":
      $rc_type = "Videos";
  break;
  default:
  $rc_type = "";
  break;
};

switch ($rc->resource_for) {
  case "1":
      $rc_resource_for = "Red Genies";
  break;
  case "2":
      $rc_resource_for = "Sales Genies";
  break;
  case "3":
      $rc_resource_for = "Customers";
  
  
};

switch ($rc->is_featured) {
  case "0":
      $rc_is_featured = "No";
  break;
  case "1":
      $rc_is_featured = "Yes";
  
  
};

switch ($rc->status) {
  case "0":
      $rc_status = "Disabled";
  break;
  case "1":
      $rc_status = "Enabled";
  
  
};
?>

                            <tr>
                              <td><strong>{{ $rc->title }}</strong></td>
                              <td><strong>{{ $rc->link }}</strong></td>
                              <td><strong>{{ $rc_type }}</strong></td>
                              <td><strong>{{ $rc_resource_for }}</strong></td>
                              <td><strong>{{ $rc_status }}</strong></td>
                             
                              
                              
							  
							  <td>
                                <!--<a href="{{ url('/red_genie/reg_links/' . $note->id) }}" class="btn btn-block btn-primary">Reg Links</a>-->
                              </td>
                              <td>
                                <a href="{{ url('admin/resource_center/' . $rc->id) }}" class="btn btn-block btn-primary">View</a>
                              </td>
                              <td>
                                <a href="{{ url('admin/resource_center/' . $rc->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              <td>
                                <form action="{{ route('resource_center.destroy', $rc->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $rcs->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

