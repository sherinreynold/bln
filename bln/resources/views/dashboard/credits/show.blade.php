@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> Red Credits For Customer: {{ @Request::segment(3) }}</div>
                    <div class="card-body">
					
					<table style="width:100%;margin-bottom:25px;">
		<tr><td><h4>Credited to</h4></td><td><h4> Credits</h4></td><td><h4> Date</h4></td></tr>	
		<tr><td><h4></h4></td><td><h4></h4></td><td><h4></h4></td></tr>
		
		@foreach($credits as $credit)
		<tr>
		<td>{{ @$credit->rg_code }}  {{ @$credit->sg_code }}</td>
		<td> {{ @$credit->credit }}</td>
		<td> {{ @$credit->created_at }}</td>
		</tr>
		@endforeach
					
					</table>
                       
                        
                        <a href="{{ route('credits.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection