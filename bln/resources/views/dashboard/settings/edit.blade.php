@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Edit') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{url('admin/settings/'.$settings->id)}}">
                            @csrf
                            @method('PUT')
                            

                            <div class="form-group row">
                                <label>Settings</label>
                                <input class="form-control" type="text" placeholder="{{ __('Settings') }}" name="settings" required autofocus value="{{ $settings->settings }}" disabled>
                            </div>
							
							<div class="form-group row">
                                <label>Value</label>
                                <input class="form-control" type="text" placeholder="{{ __('Value') }}" name="value" required autofocus value="{{ $settings->value }}">
                            </div>
							
							
							
							
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                            <a href="{{ route('settings.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection