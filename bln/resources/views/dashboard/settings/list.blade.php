@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Settings') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <!--<a href="{{ route('red_genie.create') }}" class="btn btn-primary m-2">{{ __('Add Red Genie') }}</a>-->
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Settings</th>
                            <th>Value</th>
                           
                            
							<th></th>
							
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($settings as $note)
                            <tr>
                              <td><strong>{{ $note->settings }}</strong></td>
                              <td><strong>{{ $note->value }}</strong></td>
                              
                              <td>
                                <a href="{{ url('admin/settings/' . $note->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     {{ $settings->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

