@extends('dashboard.base')

@section('content')




        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Red Points') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('points.create') }}" class="btn btn-primary m-2">{{ __('Add Red Points') }}</a>
                        </div>
                        
						<div class="row mb-3">
                    <div class="col-sm-8">
                        <form action="{{ url('admin/points') }}" methos="GET">
						<table style="width: 100%;margin-left: -8px;">
						<tr>
                            <td>
							
							<input class="form-control" type="text" placeholder="{{ __('Search Refferal/Customer Code') }}" name="search"  value="{{@Request::get('search')}}">
                            
                           </td>
						   <td><input class="form-control" type="text" id="start_date" name="start_date" placeholder="{{ __('Start Date') }}" value="{{@Request::get('start_date')}}"></td>
						   <td><input class="form-control" type="text" id="end_date" name="end_date" placeholder="{{ __('End Date') }}" value="{{@Request::get('end_date')}}"></td>
						   <td>
                            <button type="submit" class="btn btn-secondary">Filter</button>
							</td>
							</tr>
							</table>
							
                        </form>
                    </div>
                </div>
						
						
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
						    <th>Invoice no</th>
                            <th>Refferal/Customer Code</th>
							
							<th>Invoice Amount</th>
							<th>Invoice Reff no</th>
              <th style="width:10%;">Credited to</th>
                            <!-- <th>Points</th> -->
							<th>Date</th>
                            
							
							<th></th>
							<th></th>
							
                            
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($points as $credit)
						   @if($credit->rg_code != '' || $credit->sg_code != '')
                            <tr>
						      <td><strong>{{ $credit->invoice_no }}</strong></td>
                              <td><strong>{{ $credit->refferal_code }}</strong></td>
							  
							  <td><strong>{{ $credit->invoice_amount }}</strong></td>
							  <td><strong>{{ $credit->invoice_reff_id }}</strong></td>
                              <td><strong>{{ @$credit->rg_codes }}  {{ @$credit->sg_codes }}</strong></td>
                              <!-- <td>{{ $credit->point }}</td>-->
							  <td>{{ $credit->created_at}}</td>
                              
                              
							  
							  
                              <td>
                                <a href="{{ url('admin/points/' . $credit->invoice_no) }}" class="btn btn-block btn-primary" target="_blank">View Details</a>
                              </td>
                             
                              <td>
                              @if($credit->refferal_code == 'admin')
                                <a href="{{ url('admin/points/' . $credit->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                                @endif
                              </td>
                             
                              
                            </tr>
							@endif
                          @endforeach
                        </tbody>
                      </table>
                     {{ $points->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
		
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection


@section('javascript')

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
	  $( "#start_date" ).datepicker();
	  $( "#end_date" ).datepicker();
    
  } );
  
  $("#start_date").on("change",function(){
  $( "#start_date" ).datepicker("option", "dateFormat", 'yy-mm-dd');
  });
  
  $("#end_date").on("change",function(){
	$( "#end_date" ).datepicker("option", "dateFormat", 'yy-mm-dd');
  });
  
  </script>

@endsection

