@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Edit Points') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('admin/points/'.$note->id.'/update') }}">
                            @csrf
                            

                          
                            <div class="form-group row">
                                <input type="radio" name="parent" value='r' @if($note->user_type =='redgenie') checked @endif>Redgenie&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="parent" value='s' @if($note->user_type =='salesgenie') checked @endif>Salesgenie

                                

                                </div>

							
							<div class="form-group row rg">
                                <label>Red Genie</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="redgenie_id" id="rg_drp" required  autofocus>
							<option value=''>--Select Red Genie--</option>
							<?php if($redgenies)
							{
								foreach($redgenies as $redgenie)
								{
								?>
							<option value="{{ $redgenie['fk_frontend_user_id'] }}" @if($note->fk_frontend_user_id ==$redgenie['fk_frontend_user_id']) selected @endif>{{ $redgenie['first_name'] }} - {{ $redgenie['code'] }}</option>
							<?php } } ?>
							
							</select>
							</div>

                            <div class="form-group row sg_plus" style='display:none;'>
                                <label>Sales Genie</label>
               <!-- <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>-->
                            <select class="form-control" name="salesgenieplus_id" id="sg_drp"  autofocus>
							<option value=''>--Select Sales Genie--</option>
							<?php if($salesgenie_plus)
							{
								foreach($salesgenie_plus as $sgp)
								{
								?>
							<option value="{{ $sgp['fk_frontend_user_id'] }}" @if($note->fk_frontend_user_id ==$sgp['fk_frontend_user_id']) selected @endif>{{ $sgp['first_name'] }} - {{ $sgp['code'] }}</option>
							<?php } } ?>
							
							</select>
							</div>
							
							<div class="form-group row">
                                <label>Point</label>
                                <input class="form-control" type="text" placeholder="{{ __('Point') }}" name="point" required autofocus value="{{ $note->point }}" onkeypress="return isNmb(event)">
                            </div>


							
							

                           
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('points.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')
<script>
$('input[type=radio][name=parent]').change(function() {

    var val=$(this).val();
    if(val=='s'){
        $(".sg_plus").show();
        $('#sg_drp').prop('required',true);
        $('#rg_drp').removeAttr('required');
        $(".rg").hide();
       $("#sg_type").val(1);
       $("#sg_type option[value=2]").attr('disabled','disabled');
    }else{
        $(".sg_plus").hide();
        $('#sg_drp').removeAttr('required');
        $('#rg_drp').prop('required',true);
        $(".rg").show();
        $("#sg_type").val('');
        $("#sg_type option[value=2]").removeAttr('disabled');
    }

});

$("document").ready(function(){

var p_val=$('input[name="parent"]:checked').val();
if(p_val=='s'){
        $(".sg_plus").show();
        $('#sg_drp').prop('required',true);
        $('#rg_drp').removeAttr('required');
        $(".rg").hide();
       
    }else{
        $(".sg_plus").hide();
        $('#sg_drp').removeAttr('required');
        $('#rg_drp').prop('required',true);
        $(".rg").show();
        
    }
});


</script>
@endsection