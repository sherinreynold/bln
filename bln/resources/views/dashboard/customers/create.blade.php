@extends('dashboard.base')

@section('content')


        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8">
			  
			  
			  
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Create Red Genie') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('red_genie.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label> Name</label>
                                <input class="form-control" type="text" placeholder="{{ __('First Name') }}" name="f_name" required autofocus>
                            </div>
							<!--<div class="form-group row">
                                <label>Last Name</label>
                                <input class="form-control" type="text" placeholder="{{ __('Last Name') }}" name="l_name" required autofocus>
                            </div>-->
							
							<div class="form-group row">
                                <label>Email Id</label>
                                <input class="form-control" type="email" placeholder="{{ __('Email Id') }}" name="email_id" required autofocus>
                            </div>
							
							<div class="form-group row">
                                <label>Mobile</label>
                                <input class="form-control" type="mobile" placeholder="{{ __('Mobile') }}" name="mobile" required autofocus onkeypress="return isNmb(event)">
                            </div>
							
							<div class="form-group row">
                                <label>Address</label>
                                <input class="form-control" type="text" placeholder="{{ __('Address') }}" name="address" required autofocus>
                            </div>
							<div class="form-group row">
                                <label>City</label>
                                <input class="form-control" type="text" placeholder="{{ __('City') }}" name="city" required autofocus>
                            </div>
							
							<div class="form-group row">
                                <label>State</label>
                                <input class="form-control" type="text" placeholder="{{ __('State') }}" name="state" required autofocus>
                            </div>
							
							<div class="form-group row">
                                <label>Zip</label>
                                <input class="form-control" type="text" placeholder="{{ __('Zip') }}" name="zip" required autofocus onkeypress="return isNmb(event)">
                            </div>
							
							<div class="form-group row">
                                <label>Username</label>
                                <input class="form-control" type="text" placeholder="{{ __('Username') }}" name="username" required autofocus>
                            </div>
							
							<div class="form-group row">
                                <label>Password</label>
                                <input class="form-control" type="password" placeholder="{{ __('Password') }}" name="password" required autofocus>
                            </div>

                           
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('red_genie.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection