<style type="text/css">
ul{
	margin: 0px !important;
}
.login_btn{
	cursor: pointer;
}
</style>
@extends('frontend.frontend')
@section('content')
<section class="welcome">
	<div class="container-fluid">
		<div class="row"> 
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="row">
					<div class="welcome_left_img  img-flex">
						<img src="{{asset('frontend/img/login_left.png')}}" class="img-fluid ">
					</div> 
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="welcome_right">
					<!--err/success msg -->
					@if (Session::has('message'))
					<div class="alert alert-success">
						<ul>
							<li>{!! session('message') !!}</li>
						</ul>
					</div>
					@endif
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<!--err/success msg -->
					<p class="login_as_txt">{{ __('messages.Forgot Password') }}</p>
					<div class="login_as">
						<div class="login_main">
							<form method="POST" action="{{ url('/reset_password_without_token') }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="form-group">
									<input name="email" type="email" class="form-control login_main_input" placeholder="{{ __('messages.Email Id') }}" required/>
								</div>
								<div class="form-group">
									<a href="profile.html">
										<input type="submit" name="submit" value="{{ __('messages.SUBMIT') }}" class="login_btn" />
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('javascript')
@endsection
