<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title></title>
	<style>
	.main {
		width: 600px;
		height: auto;
		margin: auto;
		border: 1px solid #dedede;
		color: #4A4A4A;
	}
	.email-container {
		padding: 15px 35px 40px 35px;
	}
	.logo-div {
		margin-bottom: 25px;
		/*margin-left: 30px;*/
	}
	.main-heading {
		font-size: 29px;
		font-weight: bold;
	}
	.sub-heading {
		font-size: 15px;
		margin-bottom: 50px;
	}
	.msg-text {
		font-size: 19px;
		margin: 30px 0px;
	}
	.button-div {
		width: 100%;
	}
	.btn-3d-wrapper {
		display: block;
		padding-top: 7px;
		padding-right: 7px;
		overflow: hidden;
		position: relative;
	}
	.btn-3d-wrapper:after {
		content: "";
		right: 7px;
		display: block;
		height: 14px;
		top: 8px;
		position: absolute;
		transform: rotateZ(-132deg);
		transform-origin: left top;
	}
	.btn-3d {
		display: block;
		text-decoration: none;
		perspective: 1000px;
		border: none;
		transform-origin: right center;
		color: #fff;
		width: 100%;
	}
	.btn-3d .text {
		display: block;
		padding: 18px 25px;
		background: #8B0000;
		font-size: 26px;
		transform-origin: right top;
		font-weight: bold;
		text-align: center;
		color: #fff;
	}
	.btn-3d:after,
	.btn-3d:before {
		position: absolute;
		content: "";
		display: block;
	}
	.btn-3d:before {
		height: 7px;
		width: 100%;
		bottom: 100%;
		left: 0;
	}
	.btn-3d:after {
		width: 7px;
		height: 98%;
		left: 100%;
		top: 0;
	}
	.btn-3d:hover {
		text-decoration: none;
		color: #fff;
	}
	.btn-3d:hover .text {
		background: #8B0000;
	}
	.footer-links {
		font-size: 23px;
		font-weight: bold;
		text-decoration: none;
		color: #4A4A4A;
	}
	.footer-links:hover {
		color: #003594;
	}
	.footer-link-div {
		padding: 10px 0px;
	}
	.copyright {
		font-size: 18px;
		color: #9B9B9B;
	}
	.copyright a {
		color: #4A4A4A;
		text-decoration: none;
	}
	.copyright a:hover {
		color: #003594;
	}
	</style>
</head>
<body>
	<div class="main">
		<div class="email-container">
			<div class="logo-div">
				<img style="margin-left: 30%" src="https://staging.refertogain.com/frontend/img/logo.png" alt="" />
			</div>
			<div>
				<h1 class="main-heading">Registration Completed</h1>
				<p class="sub-heading">Hello,  Your registration was success.</p>
				<p class="sub-heading">Please login with your credentials</p>
				
					<div class="button-div">
						<div class="btn-3d-wrapper">
							<a href="{{ url('/') }}" class="btn-3d">
								<span class="text">Login</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>