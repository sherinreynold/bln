@extends('dashboard.authBase')

@section('content')

<link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">

<section class="welcome">
  
  <div class="container-fluid">
    <div class="row"> 
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<div class="row">
<div class="welcome_left_img  img-flex">
<img src="img/login_left.png" class="img-fluid ">
</div> 
</div>
</div>
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<div class="welcome_right">
<!--err/success msg -->
@if (Session::has('message'))
	<div class="alert alert-success">
        <ul>
        <li>{!! session('message') !!}</li>
		</ul>
		</div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
               @endif
			   
			   
      <!--err/success msg -->

<p class="welcome_text">Welcome</p>
<p class="welcome_title">Refer to Gain</p>
<p class="login_as_txt">
Sign In to your account</p>

<div class="login_as">

<div class="login_main">
<form method="POST" action="{{ route('login') }}">
@csrf
                          
                          <div class="form-group">
                              
                          <input class="form-control login_main_input" type="text" placeholder="{{ __('Username') }}" name="email" value="{{ old('email') }}" required autofocus></div>
                          <div class="form-group">
                              
                          <input class="form-control login_main_input" type="password" placeholder="{{ __('Password') }}" name="password" required>
                   </div>

                          <div class="form-group">
                             
                              <a href="profile.html" ><input type="submit" name="submit" class="login_btn" value="LOGIN" style="margin-top: 35px;"></a>
                          </div>




                      </form>






</div>
                          
                          </div>
                          



</div>





</div>
</div>
</div>
</div>

</section>

@endsection



@section('javascript')

@endsection