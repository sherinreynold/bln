
@extends('frontend.frontend')

@section('content')

<section class="welcome">
  
    <div class="container-fluid">
      <div class="row"> 
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
 <div class="row">
 <div class="welcome_left_img  img-flex">    
<img src="{{asset('frontend/img/login_left.png')}}" class="img-fluid "> 
</div> 
</div>
</div>
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<div class="welcome_right">


<!--err/success msg -->
		@if (Session::has('message'))
	<div class="alert alert-success">
        <ul>
        <li>{!! session('message') !!}</li>
		</ul>
		</div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
               @endif
			   
			   
      <!--err/success msg -->


<p class="welcome_text">{{ __('messages.Create Customer') }} </p>
<p class="welcome_title">Refer to Gain</p>
<div class="login_as login_btm">

<form id="login-form" class="form" action="" method="post">


<label class="radio_container d-inline">{{ __('messages.Customer') }}
  <input type="radio" value="3"  name="type" checked>
  <span class="check_mark"></span>
</label>
<label class="radio_container d-inline">{{ __('messages.Vendor') }}
  <input type="radio" value="2"  name="type" >
  <span class="check_mark"></span>
</label>
<div class="login_main">

                            
                            <div class="form-group">
                                
                                <input type="text" name="name" id="name" class="form-control login_main_input" placeholder="{{ __('messages.Name') }}" value="" required>
                            </div>
                            
                            <div class="form-group">
                                
                                <input type="text" name="email_id" id="email_id" class="form-control login_main_input"placeholder="{{ __('messages.Email Id') }}" value="" required>
                            </div>
                            <div class="form-group">
                                
                                <input type="text" name="mobile" id="mobile" class="form-control login_main_input"placeholder="{{ __('messages.Phone') }} " value="" required>
                            </div>
                           
                            
<div class="form-group">
                               
                                <input type="submit" name="submit" class="login_btn" value="{{ __('messages.Add') }}">
                            </div>



<input type="hidden" name="_token" value="{{ csrf_token() }}">




</div>

</form>


</div>





</div>
</div>
</div>
  </div>

</section>

@endsection


@section('javascript')

@endsection