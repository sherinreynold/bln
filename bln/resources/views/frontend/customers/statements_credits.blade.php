
@extends('frontend.frontend')

@section('content')

<section class="credit_div">
 
   <div class="container">
     <div class="row">
     <p class="credit_title  ct2"><a href="{{url('statements')}}">{{ __('messages.Point').' '.__('messages.Statements') }}</a></p>
     
  <p class="credit_title credit_active ct1"> <a href="#"> {{ __('messages.Credit').' '.__('messages.Statements') }}</a> </p>
     
</div>
 </div>

</section>


<section class="point_div">
  
    <div class="container">
      <div class="row">
<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
<p class="point_title">{{ __('messages.Credit').' '.__('messages.Statements') }}</p>

</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
 <div class="date_main">
 <form action='' method="post" id="frm_filter_statements">
 @csrf
<div class="date_new">
 <input type="text" placeholder="{{ __('messages.Customer code') }}" name="search" value="{{@Request::get('search')}}" class="">
</div>
<div class="date_1">
 <div class="two_fields">
      <div class="field_1">
        
        <div class="ui calendar" id="rangestart">
          <div class="ui input left icon">
           
            <input type="text" placeholder="{{ __('messages.From') }}" name="start_date" class="ui_input_1" value="{{@Request::get('start_date')}}">
          </div>
        </div>
      </div>
      <div class="field">
        
        <div class="ui calendar" id="rangeend">
          <div class="ui input left icon">
          
            <input type="text" placeholder="{{ __('messages.To') }}" name="end_date" class="ui_input_1" value="{{@Request::get('end_date')}}">
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="submit" value='filter' id="filter_btn" style="display:none;">
  </form>

</div>
<a href="#" id="apply_filter"><img src="{{asset('frontend/img/filter.png')}}" class="img-fluid filter_icon"></a>

</div>


</div>

</div>
  </div>

</section>

<!--============point_div============-->
<!--============table_sectn============-->
<section class="table_sectn" style="min-height:360px;">
  
    <div class="container">
      <div class="row">
  <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
   <!-- @php
      echo '<pre>';
      print_r($points);
      @endphp -->
<div style="overflow-x:auto;">
  <table class="stmnt_table table-responsive">
    <tr class="top_tr">
      <th class="date_sn">{{ __('messages.Date') }}</th>
      <th class="dscptn_sn tble_cnt">{{ __('messages.Description') }}</th>
      
      <th class="rp_sn text-center">{{ __('messages.Red Credits') }}</th>
      
    </tr>
    
    <!--<tr>
    <td colspan="4">There is no data to display</td>
    </tr>-->
    <?php $total_credits=0; ?>
    @foreach ($credits as $credit)
    
    <tr>
      <td class="date_sn">{{$credit['created_at']}}</td>
      <td class="tble_cnt">For customer :{{$credit['refferal_code']}}. </td>
      
      <td class="text-center rp_sn">{{$credit['credit']}}</td>
      
    </tr>
    <?php $total_credits=$total_credits+$credit['credit']; ?>
    @endforeach
    
    <tr>
       <td class="date_sn hgt"></td>
      <td><span class="tble_cnt"></span> </td>
      
      <td class="text-center rp_sn gray">{{ __('messages.Total') }}</br><span class="total">{{$total_credits}}</span></td>
      
    </tr>
  </table>
</div>
</div>


</div>
  </div>

</section>

<style>
.date_sn{
	width:250px;
}

.credit_div
{
float: left;
   width: 100%;
   height: auto;
   background-color: #F5F5F5;


}
.credit_title
{
color: #fff;
   font-size: 28px;
   padding: 16px 15px 16px 15px;
   text-align: left;
   font-weight: 400;
   line-height: 45px;
}
.credit_title a
{
color: #000;
}
 .credit_active /*.credit_title:hover,*/
{
background-color: #cc2022;
}

.credit_active a{
	color:#fff;
}

.ct2:hover
{
	background-color: #d6d4d4;
}



.date_new
{
float: left;
   
    height: auto;	
	
	}
	.date_new input
{ padding: 15px 11px 17px 10px !important;
    color: #454545 !important;
    width: 200px;
    color: #454545;
    font-size: 16px;
    border-radius: 10px;
    margin: 41px 32px 70px 0px;
}
.ui_input_1
{
  width: 200px !important;	
	
	}
	.field_1 {
    width: 200px;
    float: left;
    margin-right: 32px;
	}
	@media(max-width: 1199px) and (min-width: 992px) {
		.two_fields {
   
   margin-right: 0px;
		}
		.field_1 {
    width: 200px;
    float: left;
    margin-right: 10px;
}
}
@media(max-width: 700px) and (min-width: 320px) {
	.date_new input {
   
    margin: 41px 32px 10px 0px;
}
.filter_icon {
    float: right;
    padding: 3px 0px 37px;
}
	
	.date_new {
    float: left;
    height: auto;
    width: 70%;
}
}
</style>

@endsection


@section('javascript')
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script> 
<script>

$('#rangestart').calendar({
  type: 'date',
  endCalendar: $('#rangeend'),
  formatter: {
      date: function (date, settings) {
        if (!date) return '';
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return  year + '-' + month + '-' + day;
      }
  }
});
$('#rangeend').calendar({
  type: 'date',
  startCalendar: $('#rangestart'),
  formatter: {
      date: function (date, settings) {
        if (!date) return '';
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return  year + '-' + month + '-' + day;
      }
  }
});


$(document).ready(function(){
	$("#apply_filter").on("click",function(){
		$("#filter_btn").trigger('click');
		return false;
	});
});

</script>

@endsection