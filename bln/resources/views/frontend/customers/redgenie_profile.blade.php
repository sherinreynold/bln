
@extends('frontend.frontend')

@section('content')
<style>
.hidden
{
	display:none;
}
</style>

<link rel="stylesheet" href="{{asset('frontend/css/croppie.min.css')}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('frontend/js/croppie.js')}}"></script>

<section class="profile_sectn">
  
    <div class="container">
	<form id="login-form" class="form" action="{{url('/profile_update')}}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        
		<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"> 
		@if (file_exists(public_path('upload/profile_pics/profile_pic_'.$user["fk_frontend_user_id"].'.png')))
		<img src="{{asset('upload/profile_pics/profile_pic_'.$user['fk_frontend_user_id'].'.png')}}" class="pro_pic img-fluid">
        @else	
		<img src="{{asset('frontend/img/profile_pic_default.png')}}" class="pro_pic img-fluid">
	    @endif
		<center><a href="#" id="myBtn" style="text-align:center"><img width="35" height="30" src='{{asset("frontend/img/camera.png")}}'></a></center>
        <p class="name">{{ __('messages.Red Genie') }}</p> 
      </div>
	  
      <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="row">
<div class="pro_detai1">
<p class="pro_name"><span class="show">{{$user['first_name']}}</span >
<input type="text" name="name" id="name" class="form-control login_main_input hidden" placeholder="Name" value="{{$user['first_name']}}" required>
</p>
<p class="pro_mail"><span class="show">{{$login_info['email']}}</span>
<input type="text" name="email" id="email" class="form-control login_main_input hidden" placeholder="Email" value="{{$login_info['email']}}" required>

</p>
<p class="pro_detail"><span class="show">{{$user['address']}}
</span>
<textarea name="address" id="address" class="form-control hidden" placeholder="Address" required style="width: 82% !important;">{{$user['address']}} </textarea>

</p>     
</div>
</div>

 </div>
 
<div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
 <div class="pro_cnt_detai1">
<img onclick="edit();"src="{{asset('frontend/img/edit.png')}}" class="edit_icon show">
<input type="submit" value="{{ __('messages.save') }}" class="login_btn hidden" style="float:right;width: 100% !important;height: 37px !important;">
<p class="mob_no"><span class="mob_icon"><img src="{{asset('frontend/img/mob_icon.png')}}" class="img-fluid" ></span> <span class="no"><span class="show">+{{$user['mobile']}}</span>
<input required type="text" name="phone" id="phone" class="form-control login_main_input hidden" placeholder="Mobile" value="{{$user['mobile']}}" style="margin-top: -7px !important;
width: 82% !important;">
 </span></p>
 <p class="country"></p>
 <p class="cntry_no"><span class="cntry_icon"><img src="{{asset('frontend/img/flag.png')}}" class="img-fluid"  ></span> <span class="cntry">{{ __('messages.State of Qatar') }}</span> </p>
 <p class="country"></p>      
      </div>
</div>

</div>
</form>



  </div>

</section>
<!--============profile_sectn============-->
<!--============experience_sectn============-->
<section class="experience">
  
    <div class="container">
      <div class="row">
<div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2"></div>
<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
<div class="experience_sectn">
<p class="exp_title">{{ __('messages.Experience') }}<span class="exp_icon"><img src="{{asset('frontend/img/edit.png')}}" class="img-fluid"  onclick="add_exp();"></span></p>



<div class="exp_btm_sectn" id="add_exp_section" style="display:none;">
<form id="login-form" class="form" action="{{url('/exp_update')}}" method="POST">
@csrf
<input type="text" name="title" id="title" class="form-control login_main_input" placeholder="{{ __('messages.Designation') }}" value="" required>
</br>
<textarea name="description" id="description" class="form-control" placeholder="{{ __('messages.Description') }}" required ></textarea>
<input type="submit" style="width:25%;float:right;margin-left:10px;" value="{{ __('messages.Add') }}" class="login_btn" >
<input type="button" style="width:25%;float:right;" value="{{ __('messages.Cancel') }}" class="login_btn" onclick="cancel_exp_edit();">

</form>
</div>


@foreach ($exp as $e)
<div class="exp_btm_sectn">
<a href="{{url('/exp_remove/'.$e->id)}}" style="float:right;display:none;" class="remove_exp">{{ __('messages.Remove') }}</a>
<p class="exp_btm_title">{{$e->title}}</p>
<p class="exp_btm_dscptn founder exp_short_{{$e->id}}">{!! Str::limit($e->description, 50, ' ......') !!}</p>
<span class="exp_short_{{$e->id}}"><img src="{{asset('frontend/img/red_dpdwn.png')}}" class="acrdn"  onclick="sho_more('{{$e->id}}');"></span>
<p class="exp_btm_dscptn founder exp_more_{{$e->id}}"  style="display:none;">{{$e->description}}</p>
<span class="exp_more_{{$e->id}}" style="display:none;"><img src="{{asset('frontend/img/red_dpdwn.png')}}" class="acrdn"  onclick="sho_less('{{$e->id}}');"></span>


</div>
@endforeach

</div>
</div>

</div>
  </div>
  
  
  
  <!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    
	
	<div class="container">

<div class="panel panel-info">

  <div class="col-md-4" style="padding:5%;">

      <strong>{{ __('messages.Select image to crop') }}</strong>

      <input type="file" id="image">


      

      </div>

  <div class="panel-body">


  


    <div class="row">

      <div class="col-md-12 text-center">

      <div id="upload-demo"></div>
	  
	  

      </div>

      


      <div class="col-md-12 text-center">

      <button class="login_btn upload-image" >{{ __('messages.Upload Image') }}</button>

      </div>

    </div>


  </div>

</div>

</div>
	
	
  </div>

</div> 



<style>
 /* The Modal (background) */
 .modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 5% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 50%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
} 
</style>

</section>

<script>
function edit()
{
	$('.hidden').show();
	$('.show').hide();
}
function add_exp(){
	$("#add_exp_section , .remove_exp").show();
}

function cancel_exp_edit(){
  $("#add_exp_section , .remove_exp").hide();
}

function sho_more(elem){

$('.exp_more_'+elem).show();
$('.exp_short_'+elem).hide();
}

function sho_less(elem){

$('.exp_more_'+elem).hide();
$('.exp_short_'+elem).show();
}


//cropp

$.ajaxSetup({

headers: {

  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});


var resize = $('#upload-demo').croppie({

  enableExif: true,

  enableOrientation: true,    

  viewport: { 

      width: 200,

      height: 200,

      type: 'circle'

  },

  boundary: {

      width: 300,

      height: 300

  }

});


$('#image').on('change', function () { 

var reader = new FileReader();

  reader.onload = function (e) {

    resize.croppie('bind',{

      url: e.target.result

    }).then(function(){

      console.log('jQuery bind complete');

    });

  }

  reader.readAsDataURL(this.files[0]);

});


$('.upload-image').on('click', function (ev) {

resize.croppie('result', {

  type: 'canvas',

  size: 'viewport'

}).then(function (img) {

  $.ajax({

    url: "{{route('upload.image')}}",

    type: "POST",

    data: {"image":img},

    success: function (data) {

      html = '<img src="' + img + '" />';
      location.reload(); 
      $("#preview-crop-image").html(html);

    }

  });

});

});



//modal

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
} 
</script>

@endsection


@section('javascript')

@endsection