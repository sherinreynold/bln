
@extends('frontend.frontend')

@section('content')

<section class="welcome">
  
    <div class="container-fluid">
      <div class="row"> 
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
 <div class="row">
 <div class="welcome_left_img  img-flex">    
<img src="{{asset('frontend/img/login_left.png')}}" class="img-fluid "> 
</div> 
</div>
</div>
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<div class="welcome_right">


<!--err/success msg -->
		@if (Session::has('message'))
	<div class="alert alert-success">
        <ul>
        <li>{!! session('message') !!}</li>
		</ul>
		</div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
               @endif
			   
			   
      <!--err/success msg -->


<p class="welcome_text">Complete Registration </p>
<p class="welcome_title">Refer to Gain</p>
<div class="login_as login_btm">

<form id="login-form" class="form" action="" method="post" autocomplete="off">


<label class="radio_container d-inline">Customer
  <input type="radio" value="customer"  name="type" checked>
  <span class="check_mark"></span>
</label>
<div class="login_main">

                            
                            <div class="form-group">
                                
                                <input type="text" name="ref" id="ref" class="form-control login_main_input" placeholder="Referral ID" value="{{ @$reg_info[0]->code }}" disabled>
                            </div>
                            <div class="form-group">
                                
                                <input type="text" pattern=".{5,}" name="username" id="username" class="form-control login_main_input" placeholder="Username" required title="5 characters minimum" autocomplete="false">
                            </div>
                            <div class="form-group">
                                
                                <input type="text" name="password" id="password" class="form-control login_main_input"placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                
                                <input type="text" name="email_id" id="email_id" class="form-control login_main_input"placeholder="Email Id" value="{{ @$reg_info[0]->email }}" disabled>
                            </div>
                            <div class="form-group">
                                
                                <input type="text" name="phone" id="phone" class="form-control login_main_input"placeholder="Phone " value="{{ @$reg_info[0]->mobile }}">
                            </div>
                           <!-- <div class="form-group fgw">
                                <label class="rmbr_container">Remember me
  <input type="checkbox" class="checkmark_remember" >
  <span class="checkmark"></span>
</label><div id="register-link" class="text-right ">
                                <a href="#" class="forget_link">Forget Password?</a>
                            </div>
                            </div>-->
                            
<div class="form-group">
                               
                                <input type="submit" name="submit" class="login_btn" value="REGISTER">
                            </div>


<p class="signup_link ">You already have an account Please <a href="{{url('/')}}"><span class="red_signup">LOGIN</span></a></p>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="c_id" value="{{ @$reg_info[0]->id }}">
<input type="hidden" name="f_id" value="{{ @$reg_info[0]->fk_frontend_user_id }}">

                        






</div>

</form>


</div>





</div>
</div>
</div>
  </div>

</section>

@endsection


@section('javascript')

@endsection