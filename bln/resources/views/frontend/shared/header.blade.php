<!--============head_top============-->

<!--============head_logged in users============-->
<?php

$current_lang='English';
$to_language='Arabic';
$to_language_url=url('/locale/ar'); 
if (App::isLocale('ar')) {
$current_lang='Arabic'; 
$to_language='English';
$to_language_url=url('/locale/en');	
}


if(isset($login_info)){ ?>







  <section class="head_top">
  
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2"> 
      <a href="{{url('/')}}"><img src="{{asset('frontend/img/logo.png')}}" class="img-fluid logo"></a> 
    </div>
     <div class="col-3 col-sm-5 col-md-4 col-lg-7 col-xl-7">
          <div class="navbar-light">
      <button class="navbar-toggler navbar-toggler_r2g" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>

</button></div>
<nav class="navbar navbar-expand-lg  bg-light r2g_nav">


<div class="collapse navbar-collapse r2g_nav_item" id="navbarNavAltMarkup">
  <div class="navbar-nav ">
  
          
    <a class="nav-item nav-link @if(Request::segment(1)=='profile') 
  active menu_active
  @endif " href="{{url('profile')}}">{{ __('messages.Home') }} <span class="sr-only">(current)</span></a>
    <a class="nav-item nav-link @if(Request::segment(1)=='resource_center') 
  active menu_active
  @endif" href="{{url('resource_center')}}">{{ __('messages.Resource Centre') }}</a>
    <a class="nav-item nav-link @if(Request::segment(1)=='statements') 
  active menu_active
  @endif" href="{{url('statements')}}">{{ __('messages.Statements') }}</a>
    
  </div>
</div>
</nav>

    </div>
    <div class="col-9 col-sm-7 col-md-5 col-lg-3 col-xl-3">
      <div class="head_right">
	  @if($login_info['user_type']!='customer')
    <a href="{{ route('network') }}"><img src="{{asset('frontend/img/profile_nw.png')}}" class="img-fluid p_nw"></a>
      @endif
<a href="#">

@if (file_exists(public_path('upload/profile_pics/profile_pic_'.$login_info["id"].'.png')))
		<img width="60" height="84" src="{{asset('upload/profile_pics/profile_pic_'.$login_info['id'].'.png')}}" class=" img-fluid p_pic">
        @else	

<img src="{{asset('frontend/img/avatar1.png')}}" class="img-fluid p_pic">
@endif
</a>

<div class="profile_dropdown">
<button class="pro_dropbtn"><img src="{{asset('frontend/img/arrow_dwn.jpg')}}" class="img-fluid"></button>
<div class="dropdown-content">

@if($login_info['user_type']=='redgenie')
<a href="{{url('/create_salesgenie')}}">{{ __('messages.Create Sales Genies') }}</a>
@endif
@if($login_info['user_type']=='salesgenie')
@php
$sg = App\Models\SalesGenies::where('fk_frontend_user_id',$login_info['id'])->first()->ToArray();

@endphp

@if($sg['type']==2)
<a href="{{url('/create_salesgenie')}}">{{ __('messages.Create Sales Genies') }}</a>
@endif

<a href="{{url('/invites')}}">{{ __('messages.Invite Customers') }}</a>
@endif

<form id="login-form" class="form" action="{{ url('/logout')}}" method="post">
   <a href="#"><input type="submit" name="submit"  value="{{ __('messages.Logout') }}" style="background: none!important;border: none;padding: 0!important;"></a>
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

  </form>
	
    
  
</div>
</div>

<div class="dropdown">
<button class="btn  dropdown-toggle language_btn" type="button" data-toggle="dropdown">{{@$current_lang}} <img src="{{asset('frontend/img/lnge_drp_dwn.png')}}" class="img-fluid lnge_drp_dwn"></button>
<ul class="dropdown-menu dropdown_lng">
  <li><a href="{{@$to_language_url}}">{{@$to_language}}</a></li>
  
</ul>
</div>

</div>


</div>
  
  </div>
</div>

</section>


	
	
<?php } else {?>

<!--============head not loggedin ============-->

<section class="head_top">
  
    <div class="container">
      <div class="row">
        <div class="col-9 col-sm-6 col-md-6 col-lg-3 col-xl-3"> 
        <a href="{{url('/')}}"><img src="{{asset('frontend/img/logo.png')}}" class="img-fluid logo"> </a>
      </div>
       <div class="col-3 col-sm-6 col-md-6 col-lg-9 col-xl-9">
      <div class="dropdown">
  <button class="btn  dropdown-toggle language_btn" type="button" data-toggle="dropdown">{{@$current_lang}} <img src="{{asset('frontend/img/lnge_drp_dwn.png')}}" class="img-fluid lnge_drp_dwn"></button>
  <ul class="dropdown-menu dropdown_lng">
    <li><a href="{{@$to_language_url}}">{{@$to_language}}</a></li>
    
  </ul>
</div>

      
      </div>
    </div>
  </div>

</section>

<?php } ?>

<!--============head_top_ends============-->