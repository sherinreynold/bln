<!--============footer============-->
<section class="footer">
  
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6"><p class="ftr_left">©2020 Redlogik - All Rights Reserved</p> 
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6"><p class="ftr_right">{{ __('messages.Terms of Use') }} | {{ __('messages.Privacy Policy') }}</p> 
      </div>
    </div>
  </div>

</section>


 <!--============footer============-->
 