
@extends('frontend.frontend')

@section('content')


<section class="welcome">
  
    <div class="container-fluid">
      <div class="row row_except"> 
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
 <div class="row">
<div class="welcome_left_img  img-flex">
<img src="{{asset('frontend/img/login_left.png')}}" class="img-fluid ">
</div> 
</div>
</div>
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<div class="welcome_right">


<!--err/success msg -->
		@if (Session::has('message'))
	<div class="alert alert-success">
        <ul>
        <li>{!! session('message') !!}</li>
		</ul>
		</div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
               @endif
			   
			   
      <!--err/success msg -->



<p class="welcome_text">{{ __('messages.Welcome to our application') }}</p>
<p class="welcome_title">Refer to Gain</p>
<p class="login_as_txt">{{__('messages.LOGIN AS')}}</p>
<div class="login_as">
<label class="radio_container d-inline">{{ __('messages.Red Genie') }}
  <input type="radio"  name="radio">
  <span class="check_mark"></span>
</label>

<label class="radio_container d-inline">{{ __('messages.Sales Genie') }}
  <input type="radio"  name="radio">
  <span class="check_mark"></span>
</label>
<label class="radio_container d-inline">{{ __('messages.Customer') }}
  <input type="radio"  name="radio">
  <span class="check_mark"></span>
</label>
<div class="login_main">
<form id="login-form" class="form" action="{{ url('/login')}}" method="post">
                            
                            <div class="form-group">
                                
                                <input type="text" name="email" id="username" class="form-control login_main_input" placeholder="{{ __('messages.Username') }}" required>
                            </div>
                            <div class="form-group">
                                
                                <input type="password" name="password" id="password" class="form-control login_main_input"placeholder="{{ __('messages.Password') }}" required>
                            </div>
                            <div class="form-group fgw">
                                <label class="rmbr_container">{{ __('messages.Remember me') }}
  <input type="checkbox" class="checkmark_remember" >
  <span class="checkmark"></span>
</label><div id="register-link" class="text-right ">
                                <a href="{{ url('/forgot-password') }}"  class="forget_link">{{ __('messages.Forget Password?') }}</a>
                            </div>
                            </div>
                            
<div class="form-group">
                               
                                <a href="profile.html"><input type="submit" name="submit" class="login_btn" value="{{__('messages.LOGIN')}}"></a>
                            </div>


<p class="signup_link ">{{ __('messages.Dont have an account Please') }} <a href="{{url('/signup')}}">
<span class="red_signup">{{ __('messages.SIGN UP') }}</span></a></p>

<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        </form>






</div>


</div>





</div>
</div>
</div>
  </div>

</section>


@endsection


@section('javascript')

@endsection