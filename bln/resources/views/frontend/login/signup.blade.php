
@extends('frontend.frontend')

@section('content')


<section class="welcome">
  
    <div class="container-fluid">
      <div class="row row_except "> 
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
 <div class="row">
 <div class="welcome_left_img  img-flex">    
<img src="{{asset('frontend/img/login_left.png')}}" class="img-fluid "> 
</div> 
</div>
</div>
<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<div class="welcome_right">


<!--err/success msg -->
		@if (Session::has('message'))
	<div class="alert alert-success">
        <ul>
        <li>{!! session('message') !!}</li>
		</ul>
		</div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
               @endif
			   
			   
      <!--err/success msg -->


<p class="welcome_text">{{ __('messages.Register to') }} </p>
<p class="welcome_title">Refer to Gain</p>
<div class="login_as login_btm">

<form id="login-form" class="form" action="" method="post" autocomplete="off">

<label class="radio_container d-inline">{{ __('messages.Red Genie') }}
  <input type="radio" value="redgenie"  name="type" required @if($type == 'redgenie') checked @endif>
  <span class="check_mark"></span>
</label>

<label class="radio_container d-inline">{{ __('messages.Sales Genie') }}
  <input type="radio" value="salesgenie"  name="type" required @if($type == 'salesgenie') checked @endif>
  <span class="check_mark"></span>
</label>
<label class="radio_container d-inline">{{ __('messages.Customer') }}
  <input type="radio" value="customer"  name="type" required @if($type == 'customer') checked @endif>
  <span class="check_mark"></span>
</label>
<div class="login_main">

                            
                            <div class="form-group">
                                
                                <input style="display:none;" type="text" name="ref" id="ref" class="form-control login_main_input" placeholder="{{ __('messages.Referral ID') }}" value="{{@$ref}}">
                            </div>
                            <div class="form-group">
                                
                                <input type="text" pattern=".{5,}" name="username" id="username" class="form-control login_main_input" placeholder="{{ __('messages.Username') }}" required title="5 characters minimum" autocomplete="false" value="{{@$username}}">
                            </div>
                            <div class="form-group">
                                
                                <input type="password" name="password" id="password" class="form-control login_main_input"placeholder="{{ __('messages.Password') }}" required>
                            </div>
                            <div class="form-group">
                                
                                <input type="email" name="email_id" id="email_id" class="form-control login_main_input"placeholder="{{ __('messages.Email Id') }}" required value="{{@$email_id}}">
                            </div>
                            <div class="form-group">
                                
                                <input type="text" name="phone" id="phone" class="form-control login_main_input"placeholder="{{ __('messages.Phone') }} " required value="{{@$phone}}">
                            </div>
                            <!--<div class="form-group fgw">
                                <label class="rmbr_container">Remember me
  <input type="checkbox" class="checkmark_remember" >
  <span class="checkmark"></span>
</label><div id="register-link" class="text-right ">
                                <a href="#" class="forget_link">Forget Password?</a>
                            </div>
                            </div>-->
                            
<div class="form-group">
                               
                                <input type="submit" name="submit" class="login_btn" value="{{ __('messages.REGISTER') }}">
                            </div>


<p class="signup_link ">{{ __('messages.You already have an account Please') }} <a href="{{url('/')}}"><span class="red_signup">{{ __('messages.LOGIN') }}</span></a></p>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        






</div>

</form>


</div>





</div>
</div>
</div>
  </div>

</section>


@endsection


@section('javascript')

<script>
$('input[type=radio][name=type]').change(function() {

    var val=$(this).val();
    if(val=='salesgenie'){
        $("#ref").show();
	}else if(val=='customer'){
		$("#ref").show();
	}else{
		$("#ref").hide();
	}

});

$("document").ready(function(){

var val=$('input[name="type"]:checked').val();
if(val=='salesgenie'){
        $("#ref").show();
	}else if(val=='customer'){
		$("#ref").show();
	}else{
		$("#ref").hide();
	}
});


</script>

@endsection