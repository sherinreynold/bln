@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                @if (Request::is('dashboard'))
                My
                @endif
                
                 Companies</div>

                <div class="card-body">
                    @if ($listings)
                    @foreach($listings as $list)
                        <div class="alert alert-success" role="alert">
                        {{$list->com_name}}
                        </div>
                    @endforeach    
                    @endif

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
